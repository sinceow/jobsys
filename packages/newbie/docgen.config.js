export default {
	components: [
		"./components/address/NewbieAddress.jsx",
		"./components/button/NewbieButton.jsx",
		"./components/editor/NewbieEditor.jsx",
		"./components/form/NewbieForm.jsx",
		"./components/form-designer/NewbieFormDesigner.jsx",
		"./components/list/NewbieList.jsx",
		"./components/modal/NewbieModal.jsx",
		"./components/password/NewbiePassword.jsx",
		"./components/provider/NewbieProvider.jsx",
		"./components/search/NewbieSearch.jsx",
		"./components/table/NewbieTable.jsx",
		"./components/uploader/NewbieUploader.jsx",
	],
	outDir: "docs", // folder to save components docs in (relative to the current working directry)
	apiOptions: {
		jsx: true, // tell vue-docgen-api that your components are using JSX to avoid conflicts with TypeScript <type> syntax
	},
}
