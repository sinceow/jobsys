# Bugs

- [ ] Uploader required 检测不正常
- [ ] Select 多选 required 检测不正常
- [x] Table change page size not work
- [x] Table search doesn't reset the pagination

# Improvements

- [x] Switch 添加 true value 和 false value
- [ ] 编辑时Fetch内容添加 Loading 状态
- [ ] Table selection fixed

# Features

