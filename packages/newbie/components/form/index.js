import _NewbieForm from "./NewbieForm.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieForm = withInstall(_NewbieForm)
export default NewbieForm
