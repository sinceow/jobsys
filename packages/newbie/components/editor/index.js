import _NewbieEditor from "./NewbieEditor.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieEditor = withInstall(_NewbieEditor)
export default NewbieEditor
