import _NewbieProvider from "./NewbieProvider.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieProvider = withInstall(_NewbieProvider)
export default NewbieProvider
