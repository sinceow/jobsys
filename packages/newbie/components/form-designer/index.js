import _NewbieFormDesigner from "./NewbieFormDesigner.jsx"
import withInstall from "../../utils/withInstall"

export const NewbieFormDesigner = withInstall(_NewbieFormDesigner)
export default NewbieFormDesigner
