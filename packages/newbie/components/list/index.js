import _NewbieList from "./NewbieList.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieList = withInstall(_NewbieList)
export default NewbieList
