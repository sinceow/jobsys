import _NewbiePassword from "./NewbiePassword.jsx"
import withInstall from "../../utils/withInstall"
export const NewbiePassword = withInstall(_NewbiePassword)
export default NewbiePassword
