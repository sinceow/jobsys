import _NewbieUploader from "./NewbieUploader.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieUploader = withInstall(_NewbieUploader)
export default NewbieUploader
