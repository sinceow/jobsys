import _NewbieButton from "./NewbieButton.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieButton = withInstall(_NewbieButton)
export default NewbieButton
