import _NewbieModal from "./NewbieModal.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieModal = withInstall(_NewbieModal)
export default NewbieModal
