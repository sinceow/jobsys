import Draggable from "vuedraggable"
import withInstall from "../../utils/withInstall"

Draggable.name = "NewbieDraggable"
export const NewbieDraggable = withInstall(Draggable)
export default NewbieDraggable
