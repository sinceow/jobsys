import _NewbieSearch from "./NewbieSearch.jsx"
import withInstall from "../../utils/withInstall"
export const NewbieSearch = withInstall(_NewbieSearch)
export default NewbieSearch
