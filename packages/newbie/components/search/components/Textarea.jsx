import { Textarea } from "ant-design-vue"
import { computed, ref } from "vue"
import { find } from "lodash-es"

/**
 *
 * @param {SearchItemConfig} item
 * @param {Object} queryForm
 * @param {Object} context
 */
const render = (item, queryForm, context) => {
	const { searchProvider } = context
	let conditions = [
		{ label: "等于", value: "equal" },
		{ label: "不包含", value: "exclude" },
	]

	if (item.conditions && item.conditions.length) {
		conditions = conditions.filter((condition) => item.conditions.includes(condition.value))
	}

	const displayValue = computed(() => queryForm[item.key].value.trim().split("\n").join(","))

	const inputRef = ref(null)

	/**
	 * 根据条件收集组件实际的值
	 */
	item.collectItem = () => {
		let value = null,
			searchLabel = null

		const conditionLabel = find(conditions, { value: queryForm[item.key].condition })?.label

		value = queryForm[item.key].value?.trim() ? queryForm[item.key].value.trim().split("\n") : null
		searchLabel = value ? `${item.title}${conditionLabel}: ${value?.join(",")}` : null

		return { value, searchLabel }
	}

	const onComponentOpen = () => {
		inputRef.value?.focus()
	}

	const Component = (
		<Textarea
			ref={inputRef}
			v-model:value={queryForm[item.key].value}
			allowClear={true}
			class={`${searchProvider.inputClass || ""}`}
			style={{ width: "200px" }}
			autoSize={{ minRows: 4, maxRows: 8 }}
			placeholder={`搜索${item.title}，每行一个`}
			{...item.inputProps}
		></Textarea>
	)

	return { conditions, Component, displayValue, onComponentOpen }
}

export default render
