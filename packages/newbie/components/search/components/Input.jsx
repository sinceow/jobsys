import { Input } from "ant-design-vue"
import { computed, ref } from "vue"
import { find } from "lodash-es"

/**
 *
 * @param {SearchItemConfig} item
 * @param {Object} queryForm
 * @param {Object} context
 */
const render = (item, queryForm, context) => {
	const { searchProvider } = context

	let conditions = [
		{ label: "等于", value: "equal" },
		{ label: "不等于", value: "notEqual" },
		{ label: "包含", value: "include" },
		{ label: "不包含", value: "exclude" },
		{ label: "为空", value: "null" },
		{ label: "不为空", value: "notNull" },
	]

	if (item.conditions && item.conditions.length) {
		conditions = conditions.filter((condition) => item.conditions.includes(condition.value))
	}

	const displayValue = computed(() => queryForm[item.key].value)

	const inputRef = ref()

	/**
	 * 根据条件收集组件实际的值
	 */
	item.collectItem = () => {
		let value = queryForm[item.key].value || null,
			searchLabel = null

		const condition = find(conditions, { value: queryForm[item.key].condition })

		if (condition && ["null", "notNull"].includes(condition.value)) {
			value = null
			searchLabel = `${item.title}${condition?.label}`
		} else if (value) {
			searchLabel = `${item.title}${condition?.label}: ${value}`
		}

		return { value, searchLabel }
	}

	const onComponentOpen = () => {
		inputRef.value?.focus()
	}

	const Component = (
		<Input
			ref={inputRef}
			v-model:value={queryForm[item.key].value}
			allowClear={true}
			disabled={queryForm[item.key].condition === "null" || queryForm[item.key].condition === "notNull"}
			class={`${searchProvider.inputClass || ""}`}
			style={{ width: "200px" }}
			placeholder={`搜索${item.title}`}
			{...item.inputProps}
		></Input>
	)

	return { conditions, Component, displayValue, onComponentOpen }
}

export default render
