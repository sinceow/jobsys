import { DatePicker, RangePicker, Tag } from "ant-design-vue"
import { computed, ref } from "vue"
import dayjs from "dayjs"
import { every, find } from "lodash-es"

/**
 *
 * @param {SearchItemConfig} item
 * @param {Object} queryForm
 * @param {Object} context
 */
const render = (item, queryForm, context) => {
	const { searchProvider } = context

	let conditions = [
		{ label: "等于", value: "equal" },
		{ label: "小于", value: "lessThan" },
		{ label: "大于", value: "greaterThan" },
		{ label: "介于", value: "between" },
		{ label: "为空", value: "null" },
		{ label: "不为空", value: "notNull" },
	]

	if (item.conditions && item.conditions.length) {
		conditions = conditions.filter((condition) => item.conditions.includes(condition.value))
	}

	const inputRef = ref(null)

	const format = item.inputProps?.format || "YYYY-MM-DD"
	const picker = item.inputProps?.picker || "date"

	/**
	 * 根据条件收集组件实际的值
	 */
	item.collectItem = () => {
		let value = null,
			searchLabel = null

		const conditionLabel = find(conditions, { value: queryForm[item.key].condition })?.label

		if (queryForm[item.key].condition === "between" && queryForm[item.key].value && every(queryForm[item.key].value, dayjs.isDayjs)) {
			value = queryForm[item.key].value
			searchLabel = `${item.title}${conditionLabel}: ${value[0].format(format)} - ${value[1].format(format)} `
		} else if (queryForm[item.key].condition !== "between" && dayjs.isDayjs(queryForm[item.key].value[0])) {
			value = queryForm[item.key].value[0]
			searchLabel = `${item.title}${conditionLabel}: ${value.format(format)}`
		}
		return { value, searchLabel }
	}

	const onComponentOpen = () => {
		//暂时无法实现自动弹出日期选择面板
		inputRef.value?.focus()
	}

	const displayValue = computed(() => {
		if (queryForm[item.key].condition === "between" && queryForm[item.key].value && every(queryForm[item.key].value, dayjs.isDayjs)) {
			return [
				<Tag style={{ marginRight: 0 }}>{{ default: () => queryForm[item.key].value?.[0]?.format(format) }}</Tag>,
				<span> - </span>,
				<Tag>{{ default: () => queryForm[item.key].value?.[1]?.format(format) }}</Tag>,
			]
		}
		return queryForm[item.key].value?.[0]?.format(format)
	})

	const presets = ref([
		{
			label: "上周",
			value: dayjs().add(-7, "d"),
		},
		{
			label: "上个月",
			value: dayjs().add(-1, "month"),
		},
	])
	const rangePresets = ref([
		{
			label: "最近一周",
			value: [dayjs().add(-7, "d"), dayjs()],
		},
		{
			label: "最近两周",
			value: [dayjs().add(-14, "d"), dayjs()],
		},
		{
			label: "最近30天",
			value: [dayjs().add(-30, "d"), dayjs()],
		},
		{
			label: "最近90天",
			value: [dayjs().add(-90, "d"), dayjs()],
		},
	])

	let Component =
		queryForm[item.key].condition === "between" ? (
			<RangePicker
				ref={inputRef}
				v-model:value={queryForm[item.key].value}
				allowClear={true}
				inputReadOnly={true}
				open={true}
				presets={picker === "date" ? rangePresets.value : []}
				style={{ width: "400px" }}
				class={`${searchProvider.inputClass || ""}`}
				placeholder={["开始时间", "结束时间"]}
				getPopupContainer={(triggerNode) => triggerNode?.parentNode}
				{...item.inputProps}
			></RangePicker>
		) : (
			<DatePicker
				ref={inputRef}
				v-model:value={queryForm[item.key].value[0]}
				allowClear={true}
				open={true}
				inputReadOnly={true}
				presets={picker === "date" ? presets.value : []}
				style={{ width: "200px" }}
				class={`${searchProvider.inputClass || ""}`}
				placeholder={`搜索${item.title}`}
				getPopupContainer={(triggerNode) => triggerNode?.parentNode}
				{...item.inputProps}
			></DatePicker>
		)

	return { conditions, Component, displayValue, onComponentOpen }
}

export default render
