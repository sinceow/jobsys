# jobsys-newbie

## 2.0.0

### Major Changes

- Fix optimize

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add file uploader for form designer
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fix group
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Fix
- Optimize table
- Add group image and fix upload
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- Fix
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix uploader
- Fix

## 2.0.0

### Major Changes

- Fix optimize

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add file uploader for form designer
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fix group
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add group image and fix upload
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- Fix
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix uploader
- Fix

## 1.14.3

### Patch Changes

- Upgrate
- Fix

## 2.0.0

### Major Changes

- Fix optimize

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add file uploader for form designer
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fix group
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- Fix
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix uploader
- Fix

## 1.15.0

### Major Changes

- Fix optimize

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add file uploader for form designer
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fix group
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix uploader
- Fix

## 1.14.1

### Patch Changes

- Upgrate

## 2.0.0

### Major Changes

- Fix optimize

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add file uploader for form designer
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fix group
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.13.5

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- add modal close event
- hoolk useDateformat add 000
- fixed table search async
- fixed export
- export function auth/setDefaultPermission
- add provider form columns setting
- fixed form auto load animate
- export function auth/setDefaultpermissions
- newbie table search default condition
- add hook config status
- fixed tag time_range
- add tag timerange
- export function auth/setDefaultPermissions
- p
- fixed uploader defaultKeymap

## 1.13.4

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add file uploader for form designer
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fix group
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.13.3

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- add modal close event
- hoolk useDateformat add 000
- fixed table search async
- fixed export
- export function auth/setDefaultPermission
- add provider form columns setting
- fixed form auto load animate
- export function auth/setDefaultpermissions
- newbie table search default condition
- add hook config status
- fixed tag time_range
- add tag timerange
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.13.2

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- add modal close event
- hoolk useDateformat add 000
- fixed table search async
- fixed export
- export function auth/setDefaultPermission
- add provider form columns setting
- fixed form auto load animate
- export function auth/setDefaultpermissions
- newbie table search default condition
- add hook config status
- add tag timerange
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.14.0

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fix group
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.13.0

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Add function support for group children
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.12.0

### Minor Changes

- remove \_temp and add persistence

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.13

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix multiple file upload
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.12

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Fix collapse
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.11

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Add breakMode for form
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.10

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- add modal close event
- hoolk useDateformat add 000
- fixed table search async
- fixed export
- export function auth/setDefaultPermission
- add provider form columns setting
- fixed form auto load animate
- export function auth/setDefaultpermissions
- newbie table search default condition
- add hook config status
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.11.9

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- add modal close event
- hoolk useDateformat add 000
- fixed table search async
- export function auth/setDefaultPermission
- add provider form columns setting
- fixed form auto load animate
- export function auth/setDefaultpermissions
- newbie table search default condition
- add hook config status
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.11.8

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.7

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- modify the submit form in item beforeSubmit
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.6

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Optimize matrix checkbox
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.5

### Patch Changes

- Upgrate

## 1.11.4

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix fieldNames
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.3

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Deal with the cascade default value
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.2

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Add title for form design
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.1

### Patch Changes

- Fix required
- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.11.0

### Patch Changes

- change submit as promise
- Add matrix components
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.10.20

### Patch Changes

- change submit as promise
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Change cardSize into cardProps
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.10.19

### Patch Changes

- Upgrate

## 1.10.18

### Patch Changes

- change submit as promise
- Fix pageSize
- Change modal show to open
- Add dropdownMatchSelectWidth control for select
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.10.17

### Patch Changes

- change submit as promise
- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- #
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.10.16

### Patch Changes

- change submit as promise
- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add await for form item beforeSubmit
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.10.15

### Patch Changes

- change submit as promise
- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.10.14

### Patch Changes

- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Upgrade uploader
- Fix

## 1.10.13

### Patch Changes

- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- expose submit for NewbieForm
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.12

### Patch Changes

- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- Add number support for options
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.11

### Patch Changes

- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add method prop for table
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.10

### Patch Changes

- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix row selection default value
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.9

### Patch Changes

- Fix pageSize
- Change modal show to open
- Add set data
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.8

### Patch Changes

- Fix pageSize
- Change modal show to open
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.7

### Patch Changes

- Fix pageSize
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- upgrade table actions dropdown
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.6

### Patch Changes

- Fix pageSize
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Add table slots
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.5

### Patch Changes

- Fix pageSize
- Add table slots
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.4

### Patch Changes

- Fix pageSize
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Fix table footer style
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.3

### Patch Changes

- Fix pageSize
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix table footer
- Fix null and notNull
- Fix

## 1.10.2

### Patch Changes

- Fix pageSize
- Change extra slot into function
- Fixed
- Fix search not reset page
- Fix rowSelection type
- Add include for cascade search
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix null and notNull
- Fix

## 1.10.1

### Patch Changes

- Fix pageSize
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Fixed
- Optimize table
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix null and notNull
- Fix

## 1.10.0

### Minor Changes

- Imporve the newbie search render logic in table

### Patch Changes

- Add Cascasder fieldName support
- Fix cascader changeOnSelect
- Fix customize table columns
- Fix cascader changeOnSelect

## 1.9.20

### Patch Changes

- Add Cascasder fieldName support
- Fix cascader changeOnSelect
- Fix customize table columns
- Fix cascader changeOnSelect

## 1.9.19

### Patch Changes

- Add Cascasder fieldName support
- Fix cascader changeOnSelect
- Fix customize table columns

## 1.9.18

### Patch Changes

- Add Cascasder fieldName support
- Fix customize table columns

## 1.9.17

### Patch Changes

- Fix customize table columns

## 1.9.16

### Patch Changes

- Fix pageSize
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Fixed
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix null and notNull
- Fix

## 1.9.15

### Patch Changes

- Add sortable for search

### Patch Changes

- Fix between
- Add formItems for designer

## 1.9.14

### Patch Changes

- Fix pageSize
- Change extra slot into function
- Fixed
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Fixed
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix null and notNull

## 1.9.13

### Patch Changes

- Change extra slot into function
- Fixed
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Fixed
- Add style for list
- Add sortOrder and flexible for newbie search
- useFindPropertyRecursive
- Fix null and notNull

## 1.9.12

### Patch Changes

- Change extra slot into function
- Fixed
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Fixed
- Add style for list
- useFindPropertyRecursive
- Fix null and notNull

## 1.9.11

### Patch Changes

- Change extra slot into function
- Fixed
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Fixed
- Add style for list
- useFindPropertyRecursive

## 1.9.10

### Patch Changes

- Change extra slot into function
- Fixed
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Add style for list
- useFindPropertyRecursive

## 1.9.9

### Patch Changes

- Change extra slot into function
- Fix search not reset page
- Add include for cascade search
- Add row selection width
- Add style for list
- useFindPropertyRecursive

## 1.9.8

### Patch Changes

- Change extra slot into function
- Add include for cascade search
- Add row selection width
- Add style for list
- useFindPropertyRecursive

## 1.9.7

### Patch Changes

- Upgrate

## 1.9.6

### Patch Changes

- Change extra slot into function
- Add include for cascade search
- Add row selection width
- Add style for list

## 1.9.4

### Patch Changes

- Change extra slot into function
- Add include for cascade search
- Add row selection width

## 1.9.3

### Patch Changes

- Upgrate

## 1.9.2

### Patch Changes

- Upgrate

## 1.9.1

### Patch Changes

- Upgrate

## 1.9.0

### Minor Changes

- Add sortable for search

### Patch Changes

- Add formItems for designer

## 1.8.35

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- add modal close event
- hoolk useDateformat add 000
- fixed table search async
- export function auth/setDefaultPermission
- add provider form columns setting
- fixed form auto load animate
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.8.34

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- add modal close event
- hoolk useDateformat add 000
- fixed table search async
- export function auth/setDefaultPermission
- fixed form auto load animate
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.8.33

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- hoolk useDateformat add 000
- fixed table search async
- export function auth/setDefaultPermission
- fixed form auto load animate
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.8.32

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- improved hooks/datetime
- hoolk useDateformat add 000
- export function auth/setDefaultPermission
- fixed form auto load animate
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.8.31

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- hoolk useDateformat add 000
- export function auth/setDefaultPermission
- fixed form auto load animate
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions
- fixed uploader defaultKeymap

## 1.8.30

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- hoolk useDateformat add 000
- export function auth/setDefaultPermission
- fixed form auto load animate
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions

## 1.8.29

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- hoolk useDateformat add 000
- export function auth/setDefaultPermission
- fixed form auto load animate
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions

## 1.8.28

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- hoolk useDateformat add 000
- export function auth/setDefaultPermission
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions

## 1.8.27

### Patch Changes

- fixed form item extra layout
- fixed table customize columns
- export function auth/setDefaultPermission
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions

## 1.8.26

### Patch Changes

- Change extra slot into function
- Add include for cascade search

## 1.8.25

### Patch Changes

- fixed form item extra layout
- export function auth/setDefaultPermission
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions

## 1.8.24

### Patch Changes

- export function auth/setDefaultPermission
- export function auth/setDefaultpermissions
- export function auth/setDefaultPermissions

## 1.8.23

### Patch Changes

- export function auth/setDefaultPermission
- export function auth/setDefaultpermissions

## 1.8.22

### Patch Changes

- export function auth/setDefaultPermission

## 1.8.21

### Patch Changes

- Upgrate

## 1.8.20

### Patch Changes

- Upgrate

## 1.8.19

### Patch Changes

- Add include for cascade search

## 1.8.18

### Patch Changes

- Upgrate

## 1.8.17

### Patch Changes

- Upgrate

## 1.8.16

### Patch Changes

- Upgrate

## 1.8.15

### Patch Changes

- Add hideButtons
- Add init for form item
- Change render order
- Add useFindParentValues
- Add beforeSubmit for form item
- Upgrade form designer
- Add slots for form item
- Add field item pattern suppoer
- Add cascade for search
- remove debugger
- Add switch default props
- Add readonly
- Remove the constrait for switch default value

## 1.8.14

### Patch Changes

- Add hideButtons
- Add init for form item
- Change render order
- Add useFindParentValues
- Add beforeSubmit for form item
- Upgrade form designer
- Add slots for form item
- Add field item pattern suppoer
- Add cascade for search
- remove debugger
- Add switch default props
- Add readonly
- Remove the constrait for switch default value

## 1.8.13

### Patch Changes

- Add hideButtons
- Add init for form item
- Change render order
- Add useFindParentValues
- Add beforeSubmit for form item
- Upgrade form designer
- Add slots for form item
- Add cascade for search
- remove debugger
- Add switch default props
- Add readonly
- Remove the constrait for switch default value

## 1.8.12

### Patch Changes

- Add hideButtons
- Add init for form item
- Change render order
- Add useFindParentValues
- Add beforeSubmit for form item
- Add slots for form item
- Add cascade for search
- remove debugger
- Add switch default props
- Add readonly
- Remove the constrait for switch default value

## 1.8.11

### Patch Changes

- Add hideButtons
- Add init for form item
- Change render order
- Add useFindParentValues
- Add beforeSubmit for form item
- Add slots for form item
- Add cascade for search
- remove debugger
- Add switch default props
- Add readonly

## 1.8.10

### Patch Changes

- Add hideButtons
- Add init for form item
- Change render order
- Add useFindParentValues
- Add beforeSubmit for form item
- Add slots for form item
- Add cascade for search
- remove debugger
- Add readonly

## 1.8.9

### Patch Changes

- Add hideButtons
- Add init for form item
- Change render order
- Add useFindParentValues
- Add slots for form item
- Add cascade for search
- remove debugger
- Add readonly

## 1.8.8

### Patch Changes

- Add hideButtons
- Change render order
- Add useFindParentValues
- Add slots for form item
- Add cascade for search
- remove debugger
- Add readonly

## 1.8.7

### Patch Changes

- Add hideButtons
- Change render order
- Add slots for form item
- Add cascade for search
- remove debugger
- Add readonly

## 1.8.6

### Patch Changes

- Add hideButtons
- Change render order
- Add slots for form item
- Add cascade for search
- Add readonly

## 1.8.5

### Patch Changes

- Add hideButtons
- Change render order
- Add cascade for search
- Add readonly

## 1.8.4

### Patch Changes

- Add hideButtons
- Add cascade for search
- Add readonly

## 1.8.3

### Patch Changes

- Add cascade for search
- Add readonly

## 1.8.2

### Patch Changes

- Add cascade for search
