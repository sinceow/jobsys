# Newbie

基于 AntD Vue3 的增强组件

## 安装

使用 `npm`

```bash
npm i "jobsys-newbie"
```

使用 `yarn`

```bash
yarn i "jobsys-newbie"
```

使用 `pnpm`

```bash
pnpm i "jobsys-newbie"
```

## 示例

引入样式

```js
//app.js
import "jobsys-newbie/dist/style.css" 
```

```js
//app.js
import { createApp } from "vue"
import Newbie from "jobsys-newbie"

const app = createApp({
    setup: () => {}
})

app.use(Newbie).mount("#app")
```

```vue
//component.vue
<template>
   <NewbiePassword v-model:value="password"></NewbiePassword>
</template>
<script setup>
    import { ref } from "vue"
    
    const password = ref("")
</script>
```