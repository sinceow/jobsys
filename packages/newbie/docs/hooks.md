## Modules

<dl>
<dt><a href="#module_Cipher">Cipher</a></dt>
<dd></dd>
<dt><a href="#module_Datetime">Datetime</a></dt>
<dd></dd>
<dt><a href="#module_Form">Form</a></dt>
<dd></dd>
<dt><a href="#module_Interact">Interact</a></dt>
<dd></dd>
<dt><a href="#module_Network">Network</a></dt>
<dd></dd>
<dt><a href="#module_Uitls">Uitls</a></dt>
<dd></dd>
</dl>

<a name="module_Cipher"></a>

## Cipher

* [Cipher](#module_Cipher)
    * [.useSm2()](#module_Cipher.useSm2)
    * [.useSm3(msg, [options])](#module_Cipher.useSm3) ⇒ <code>\*</code>
    * [.useSm4Encrypt(msg, key, [options])](#module_Cipher.useSm4Encrypt) ⇒ <code>Uint8Array</code>
    * [.useSm4Decrypt(encryptData, key, [options])](#module_Cipher.useSm4Decrypt) ⇒ <code>Uint8Array</code>

<a name="module_Cipher.useSm2"></a>

### Cipher.useSm2()
国密 sm2

**Kind**: static method of [<code>Cipher</code>](#module_Cipher)  
<a name="module_Cipher.useSm3"></a>

### Cipher.useSm3(msg, [options]) ⇒ <code>\*</code>
国密 sm3 加密

**Kind**: static method of [<code>Cipher</code>](#module_Cipher)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| msg | <code>string</code> |  | 密码信息 |
| [options] | <code>Object</code> | <code>{}</code> |  |

<a name="module_Cipher.useSm4Encrypt"></a>

### Cipher.useSm4Encrypt(msg, key, [options]) ⇒ <code>Uint8Array</code>
国密 sm4 加密

**Kind**: static method of [<code>Cipher</code>](#module_Cipher)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| msg | <code>string</code> |  | 密码信息 |
| key | <code>string</code> |  | 秘钥 |
| [options] | <code>Object</code> | <code>{}</code> |  |

<a name="module_Cipher.useSm4Decrypt"></a>

### Cipher.useSm4Decrypt(encryptData, key, [options]) ⇒ <code>Uint8Array</code>
国密 sm4 解密

**Kind**: static method of [<code>Cipher</code>](#module_Cipher)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| encryptData | <code>string</code> |  | 加密信息 |
| key | <code>string</code> |  | 秘钥 |
| [options] | <code>Object</code> | <code>{}</code> |  |

<a name="module_Datetime"></a>

## Datetime

* [Datetime](#module_Datetime)
    * [.useDayjs(date, [format])](#module_Datetime.useDayjs) ⇒ <code>dayjs.Dayjs</code>
    * [.useDateFormat(date, [format])](#module_Datetime.useDateFormat) ⇒ <code>string</code>
    * [.useDateUnix(date)](#module_Datetime.useDateUnix) ⇒ <code>number</code> \| <code>string</code>

<a name="module_Datetime.useDayjs"></a>

### Datetime.useDayjs(date, [format]) ⇒ <code>dayjs.Dayjs</code>
根据格式创建 Dayjs 对象

**Kind**: static method of [<code>Datetime</code>](#module_Datetime)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| date | <code>string</code> \| <code>number</code> |  | 日期字符 |
| [format] | <code>string</code> | <code>null</code> | 日期格式 |

<a name="module_Datetime.useDateFormat"></a>

### Datetime.useDateFormat(date, [format]) ⇒ <code>string</code>
格式化日期

**Kind**: static method of [<code>Datetime</code>](#module_Datetime)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| date | <code>dayjs.Dayjs</code> \| <code>Date</code> |  | 日期对象 |
| [format] | <code>string</code> | <code>&quot;YYYY-MM-DD HH:mm&quot;</code> | 日期格式 |

<a name="module_Datetime.useDateUnix"></a>

### Datetime.useDateUnix(date) ⇒ <code>number</code> \| <code>string</code>
获取日期的 Unix 时间戳

**Kind**: static method of [<code>Datetime</code>](#module_Datetime)  

| Param | Type | Description |
| --- | --- | --- |
| date | <code>dayjs.Dayjs</code> \| <code>Date</code> | 日期对象 |

<a name="module_Form"></a>

## Form

* [Form](#module_Form)
    * [.formLabel](#module_Form.formLabel)
    * [.useHiddenForm(options)](#module_Form.useHiddenForm) ⇒ <code>HTMLFormElement</code>
    * [.useProcessStatus(res, ops)](#module_Form.useProcessStatus)
    * [.useProcessStatusSuccess(res, success)](#module_Form.useProcessStatusSuccess)
    * [.useFormFail(e)](#module_Form.useFormFail)
    * [.useFormFormat(form, [format])](#module_Form.useFormFormat) ⇒ <code>Object</code>

<a name="module_Form.formLabel"></a>

### Form.formLabel
表单布局

**Kind**: static constant of [<code>Form</code>](#module_Form)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| formLabel.commonLabelCol | <code>Object</code> | `{ span: 8, xxl: 6 }` |
| formLabel.commonWrapperCol | <code>Object</code> | `{ span: 12, xxl: 14 }` |
| formLabel.commonWrapperOffset | <code>Object</code> | `{ xs: { offset: 8, span: 12 }, xxl: { offset: 6, span: 14 } }` |
| formLabel.commonLabelFullCol | <code>Object</code> | `{ span: 8, xxl: 6 }` |
| formLabel.commonWrapperFullCol | <code>Object</code> | `{ span: 16, xxl: 18 }` |
| formLabel.commonWrapperFullOffset | <code>Object</code> | `{ xs: { offset: 8, span: 16 }, xxl: { offset: 6, span: 18 } }` |
| formLabel.commonLabelPartCol | <code>Object</code> | `{ span: 4, xxl: 3 }` |
| formLabel.commonWrapperPartCol | <code>Object</code> | `{ span: 20, xxl: 21 }` |
| formLabel.commonWrapperPartOffset | <code>Object</code> | `{ xs: { offset: 4, span: 20 }, xxl: { offset: 3, span: 21 } }` |

<a name="module_Form.useHiddenForm"></a>

### Form.useHiddenForm(options) ⇒ <code>HTMLFormElement</code>
创建一个隐藏的表单

**Kind**: static method of [<code>Form</code>](#module_Form)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| options | <code>Object</code> |  |  |
| options.url | <code>string</code> |  | 请求地址 |
| options.data | <code>Object</code> |  | 发送的数据 |
| [options.method] | <code>string</code> | <code>&quot;post&quot;</code> | 请求方法 |
| [options.csrfToken] | <code>string</code> | <code>null</code> | CSRF Token |

<a name="module_Form.useProcessStatus"></a>

### Form.useProcessStatus(res, ops)
处理请求结果

**Kind**: static method of [<code>Form</code>](#module_Form)  

| Param | Type | Description |
| --- | --- | --- |
| res | <code>object</code> | 请求结果 |
| res.status | <code>string</code> | 请求结果状态 |
| res.result | <code>\*</code> | 请求结果信息 |
| ops | <code>Object.&lt;string, (string\|function())&gt;</code> | 状态的处理对象 |

<a name="module_Form.useProcessStatusSuccess"></a>

### Form.useProcessStatusSuccess(res, success)
处理正确请求结果

**Kind**: static method of [<code>Form</code>](#module_Form)  

| Param | Type | Description |
| --- | --- | --- |
| res | <code>object</code> | 请求结果 |
| res.status | <code>string</code> | 请求结果状态 |
| res.result | <code>\*</code> | 请求结果信息 |
| success | <code>string</code> \| <code>function</code> | 状态的处理对象 |

<a name="module_Form.useFormFail"></a>

### Form.useFormFail(e)
处理表单提交失败

**Kind**: static method of [<code>Form</code>](#module_Form)  

| Param | Type |
| --- | --- |
| e | <code>\*</code> | 

<a name="module_Form.useFormFormat"></a>

### Form.useFormFormat(form, [format]) ⇒ <code>Object</code>
处理表单数据

**Kind**: static method of [<code>Form</code>](#module_Form)  

| Param | Type | Description |
| --- | --- | --- |
| form | <code>Object</code> |  |
| [format] | <code>Object</code> | 需要处理的类型 |
| [format.date] | <code>boolean</code> \| <code>string</code> \| <code>function</code> | `true`: 转成时间戳，`string`: 为 Format 格式, 如 `"YYYY-MM-DD"`, `function`: 自定义处理函数, 参数为 dayjs 对象 |
| [format.boolean] | <code>boolean</code> | 布尔值处理, 如果开启则 `true` 转成 1, `false` 转成 0 |
| [format.attachment] | <code>string</code> \| <code>function</code> | `string`: 附件字段名, `function`: 自定义处理函数, 参数为附件对象 |

<a name="module_Interact"></a>

## Interact
<a name="module_Interact.useModalConfirm"></a>

### Interact.useModalConfirm(msg, [onOk], [loading], [onCancel]) ⇒ <code>\*</code>
确认弹窗

**Kind**: static method of [<code>Interact</code>](#module_Interact)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| msg | <code>string</code> |  | 提示信息 |
| [onOk] | <code>function</code> | <code></code> | 确认回调 |
| [loading] | <code>boolean</code> | <code>false</code> | 是否异步加载 |
| [onCancel] | <code>function</code> | <code></code> | 取消回调 |

<a name="module_Network"></a>

## Network

* [Network](#module_Network)
    * _static_
        * [.STATUS](#module_Network.STATUS)
        * [.useFetch([fetcher])](#module_Network.useFetch) ⇒ <code>Object</code>
        * [.usePage(pagination, refresh, [process])](#module_Network.usePage) ⇒ <code>object</code>
    * _inner_
        * [~get(url, [config])](#module_Network..get) ⇒ <code>Promise</code>
        * [~post(url, data, [config])](#module_Network..post) ⇒ <code>Promise</code>

<a name="module_Network.STATUS"></a>

### Network.STATUS
请求返回状态码

**Kind**: static constant of [<code>Network</code>](#module_Network)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| STATUS.STATE_CODE_SUCCESS | <code>string</code> | `SUCCESS` 成功 |
| STATUS.STATE_CODE_FAIL | <code>string</code> | `FAIL` 失败 |
| STATUS.STATE_CODE_NOT_FOUND | <code>string</code> | `NOT_FOUND` 找不到资源 |
| STATUS.STATE_CODE_INFO_NOT_COMPLETE | <code>string</code> | `INCOMPLETE` 信息不完整 |
| STATUS.STATE_CODE_NOT_ALLOWED | <code>string</code> | `NOT_ALLOWED` 无权限 |

<a name="module_Network.useFetch"></a>

### Network.useFetch([fetcher]) ⇒ <code>Object</code>
通用 AJAX 请求

**Kind**: static method of [<code>Network</code>](#module_Network)  

| Param | Type | Description |
| --- | --- | --- |
| [fetcher] | <code>Object</code> | 用于存储请求状态的对象 |

<a name="module_Network.usePage"></a>

### Network.usePage(pagination, refresh, [process]) ⇒ <code>object</code>
分页请求

**Kind**: static method of [<code>Network</code>](#module_Network)  
**Returns**: <code>object</code> - pagination 分页对象  

| Param | Type | Description |
| --- | --- | --- |
| pagination | <code>object</code> | 分页对象 |
| pagination.uri | <code>string</code> | 请求地址 |
| [pagination.params] | <code>object</code> | 请求参数 |
| [pagination.page] | <code>number</code> | 当前页码 |
| pagination.finishedText | <code>string</code> | 加载完毕文本 |
| pagination.loading | <code>boolean</code> | 加载状态 |
| pagination.finished | <code>boolean</code> | 加载完毕 |
| pagination.error | <code>boolean</code> | 是否错误 |
| pagination.errorText | <code>string</code> | 错误文本 |
| pagination.empty | <code>boolean</code> | 是否无内容 |
| pagination.items | <code>array</code> | 分页数据 |
| refresh | <code>boolean</code> | 是否刷新 |
| [process] | <code>function</code> | 数据处理函数 |

<a name="module_Network..get"></a>

### Network~get(url, [config]) ⇒ <code>Promise</code>
get请求

**Kind**: inner method of [<code>Network</code>](#module_Network)  

| Param | Type | Description |
| --- | --- | --- |
| url |  |  |
| [config] | <code>Object</code> | axios config |

<a name="module_Network..post"></a>

### Network~post(url, data, [config]) ⇒ <code>Promise</code>
post请求

**Kind**: inner method of [<code>Network</code>](#module_Network)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> |  |
| data | <code>Object</code> |  |
| [config] | <code>Object</code> | axios config |

<a name="module_Uitls"></a>

## Uitls

* [Uitls](#module_Uitls)
    * [.useLabelFromOptionsValue(value, options)](#module_Uitls.useLabelFromOptionsValue) ⇒ <code>string</code>
    * [.useFindLabelsInValues(options, values, [adapter])](#module_Uitls.useFindLabelsInValues) ⇒
    * [.useFindLabelsFromPath(options, path, [adapter])](#module_Uitls.useFindLabelsFromPath) ⇒
    * [.useFindParentLabels(options, value, adapter)](#module_Uitls.useFindParentLabels) ⇒ <code>\*</code>
    * [.useFindParentValues(options, value, adapter)](#module_Uitls.useFindParentValues) ⇒ <code>\*</code>

<a name="module_Uitls.useLabelFromOptionsValue"></a>

### Uitls.useLabelFromOptionsValue(value, options) ⇒ <code>string</code>
从 options 中根据 value 获取 label

**Kind**: static method of [<code>Uitls</code>](#module_Uitls)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> \| <code>number</code> | 值 |
| options | <code>Array</code> | 选项 |

<a name="module_Uitls.useFindLabelsInValues"></a>

### Uitls.useFindLabelsInValues(options, values, [adapter]) ⇒
从嵌套的 options 中根据 value 获取 label, 如 [1, 3] => ["东", "南"]

**Kind**: static method of [<code>Uitls</code>](#module_Uitls)  
**Returns**: Array  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| options | <code>Array</code> |  | 嵌套的选项 |
| values | <code>Array</code> |  | 需要查找的值 |
| [adapter] | <code>Object</code> | <code>{value: &quot;value&quot;,label: &quot;label&quot;,children: &quot;children&quot;}</code> | 选项适配器 |

<a name="module_Uitls.useFindLabelsFromPath"></a>

### Uitls.useFindLabelsFromPath(options, path, [adapter]) ⇒
从嵌套的 options 中根据 value 获取 label 路径, 如地区路径: [440000, 440100, 440113] => ["广东省", "广州市", " 番禺区"]

**Kind**: static method of [<code>Uitls</code>](#module_Uitls)  
**Returns**: Array  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| options | <code>Array</code> |  | 嵌套的选项 |
| path | <code>Array</code> |  | 需要查找的值的路径数组 |
| [adapter] | <code>Object</code> | <code>{value: &quot;value&quot;,label: &quot;label&quot;,children: &quot;children&quot;}</code> | 选项适配器 |

<a name="module_Uitls.useFindParentLabels"></a>

### Uitls.useFindParentLabels(options, value, adapter) ⇒ <code>\*</code>
从嵌套的 options 中根据 value 获取 label 路径, 如地区路径: 440113 => ["广东省", "广州市", " 番禺区"]

**Kind**: static method of [<code>Uitls</code>](#module_Uitls)  

| Param |
| --- |
| options | 
| value | 
| adapter | 

<a name="module_Uitls.useFindParentValues"></a>

### Uitls.useFindParentValues(options, value, adapter) ⇒ <code>\*</code>
从嵌套的 options 中根据 value 获取整个 values 路径, 如地区路径: 440113 => [440000, 440100, 440113]

**Kind**: static method of [<code>Uitls</code>](#module_Uitls)  

| Param |
| --- |
| options | 
| value | 
| adapter | 

