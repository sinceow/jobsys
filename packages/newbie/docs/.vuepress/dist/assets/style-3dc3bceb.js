import{i as r}from"./isNull-4293306c.js";import{i as e}from"./isUndefined-aa0326a0.js";const o=i=>r(i)||e(i)?"auto":isNaN(i)?i:`${i}px`;export{o as g};
