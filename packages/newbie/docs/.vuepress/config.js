import { defaultTheme, defineUserConfig, viteBundler } from "vuepress"
import vueJsx from "@vitejs/plugin-vue-jsx"
import { resolve } from "path"
import { globbySync } from "globby"

const sidebarComponents = globbySync("components/**/*.md", { cwd: resolve(__dirname, "../") })

export default defineUserConfig({
	base: "/docs/newbie",
	lang: "zh-CN",
	title: "Newbie Component",
	description: "Enhanced Vue components based on AntD.",
	bundler: viteBundler({
		viteOptions: {
			plugins: [vueJsx()],
			resolve: {
				alias: {
					"@components": resolve(__dirname, "../../components"),
				},
			},
		},
	}),
	theme: defaultTheme({
		displayAllHeaders: true,
		navbar: [
			{ text: "首页", link: "/" },
			{
				text: "组件",
				children: sidebarComponents.map((comp) => {
					return {
						text: comp
							.replace(/^components\//, "")
							.replace(/\.md$/, "")
							.split("/")[1],
						link: "/" + comp.replace(/\.md$/, ""),
					}
				}),
			},
			{
				text: "组合式 API",
				link: "/hooks",
			},
		],
	}),
})
