# NewbieFormDesigner

> 表单设计组件

Version: 1.2.0

## Props

| Prop name | Description                                                                                                                                                                           | Type           | Values | Default     |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- | ------ | ----------- |
| widgets   | 额外的业务组件<br/>`@typedef` {Object\|Array} FormDesignerWidgetGroup<br/>`@property` [title] 分组标题<br/>`@property` 组件类型                                                       | array          | -      | []          |
| formItems | 表单项<br/>`@typedef` {Object\|Array} FormDesignerFormItemConfig<br/>`@property` 表单项的唯一标识<br/>`@property` 表单项的类型<br/>`@property` 表单项的标题<br/>`@property` undefined | array\|func    | -      | () =&gt; [] |
| height    | 组件高度                                                                                                                                                                              | string\|number | -      | "100%"      |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| submit     |            |

---

## 详细参数

### `widget`

| Prop name | Description    | Type   | Values | Default |
| --------- | -------------- | ------ | ------ | ------- |
| name      | 组件名称       | string | -      | ""      |
| type      | 组件实际类型   | string | -      | ""      |
| icon      | 组件图标       | string | -      | ""      |
| props     | 组件的预设配置 | obj    | -      | null    |

```json
{
    name: "年龄",
    type: "number",
    icon: h(SmileOutlined),
    props: {
        min: 18
    }
}
```

## 示例

<script setup>
import {ref} from "vue";
import NewbieFormDesigner from "@components/form-designer/NewbieFormDesigner.jsx";

const widgets = ref([]);

const onSubmit = (form) => {
    console.log(form)
}
</script>

<NewbieFormDesigner height="500" :widgets="widgets" @submit="onSubmit"/>

```vue
<script setup>
import NewbieFormDesigner from "jobsys-newbie"

const widgets = ref([])

const onSubmit = (form) => {
	console.log(form)
}
</script>

<NewbieFormDesigner height="500" :widgets="widgets" @submit="onSubmit" />
```
