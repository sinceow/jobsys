# NewbieTable

> Newbie 表格组件

Version: 1.0.0

## Props

| Prop name       | Description                                                       | Type            | Values | Default     |
| --------------- | ----------------------------------------------------------------- | --------------- | ------ | ----------- |
| title           | 表格标题                                                          | string          | -      | ""          |
| filterable      | 是否使用表单搜索                                                  | boolean         | -      | true        |
| tableProps      | 原生表格属性                                                      | object          | -      | {}          |
| tableEvents     | 原生表格事件                                                      | object          | -      | {}          |
| pagination      | 是否使用分页，为 Object 时时使用自定义分页                        | boolean\|object | -      | true        |
| pageEvents      | 原生翻页事件                                                      | object          | -      | {}          |
| autoQuery       | 是否在搜索条件变化时自动搜索                                      | boolean         | -      | false       |
| url             | 表格数据请求 URL                                                  | string          | -      | ""          |
| extraData       | 请求数据时额外提交的参数                                          | object          | -      | {}          |
| afterFetched    | 请求后 Res 的处理方法, 如有 url， 则该方法必须传                  | func            | -      | null        |
| columns         | 表格列定义, [TableColumnConfig ](#tablecolunmconfig-配置)         | array\|func     | -      | () =&gt; [] |
| rowSelection    | 选择功能的配置                                                    | boolean\|object | -      | false       |
| rowKey          | 数据值需要指定 key 值，当启用 `rowSelection` 时一定要指定这个 key | string\|func    | -      | "id"        |
| formData        | 表格数据                                                          | array           | -      | []          |
| expandRender    | 额外的展开行                                                      | string\|func    | -      | null        |
| expandedRowKeys | 展开的行                                                          | array           | -      | []          |
| showRefresh     | 是否显示刷新按钮                                                  | boolean         | -      | true        |

## Events

| Event name | Properties | Description                   |
| ---------- | ---------- | ----------------------------- |
| fetch      |            | 未传入 `url` 时的手动请求方法 |

---

## 详细参数

### `TableColumnConfig` 配置

| Prop name      | Description                                                                                                                                                                                                               | Type             | Values | Default |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- | ------ | ------- |
| title          | 列标题                                                                                                                                                                                                                    | string           | -      | ""      |
| dataIndex      | 列数据在数据项中对应的 key，支持 a.b.c 的嵌套写法                                                                                                                                                                         | string           | -      | ""      |
| key            | Vue 需要的 key，如果已经设置了唯一的 dataIndex，可以忽略这个属性                                                                                                                                                          | string           | -      | ""      |
| width          | 列宽度                                                                                                                                                                                                                    | string           | -      | ""      |
| align          | 对齐方式                                                                                                                                                                                                                  | string           | -      | ""      |
| ellipsis       | 是否自动缩略                                                                                                                                                                                                              | boolean          | -      | ""      |
| fixed          | 列是否固定，可选 true(等效于 left) 'left' 'right'                                                                                                                                                                         | boolean          | -      | ""      |
| filterable     | 是否可过滤，如果是对象，则为过滤配置，为 true 时 `filterable.type = 'input'` <br /> [详细配置见 Search](/components/search/NewbieSearch.html#searchitemconfig-配置), key 与 title 如果不传,会直接使用 Column 的这两个属性 | boolean \|Object | -      | ""      |
| isOnlyForQuery | 是否只用于搜索                                                                                                                                                                                                            | boolean          | -      | ""      |
