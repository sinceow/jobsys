# NewbieList

> 列表组件

Version: 1.0.0

## Props

| Prop name    | Description                                                                                | Type    | Values | Default        |
| ------------ | ------------------------------------------------------------------------------------------ | ------- | ------ | -------------- |
| url          | 加载数据的 URL                                                                             | string  | -      | ""             |
| extraData    | 请求附带参数                                                                               | object  | -      | {}             |
| height       | 高度                                                                                       | number  | -      | 300            |
| offset       | 加载触发距离，滚动条与底部距离小于 offset 时触发 load 事件                                 | number  | -      | 50             |
| finishedText | 加完完毕提示文案                                                                           | string  | -      | "数据加载完毕" |
| autoLoad     | 是否自动加载                                                                               | boolean | -      | true           |
| useStore     | 是否使用 store<br/>如果使用 store, 请确认 store 中定义了 pagination 和 initPagination 方法 | object  | -      | () =&gt; null  |
| listProps    | 原生 [List](https://www.antdv.com/components/list-cn#api) 参数                             | object  | -      | {}             |

---
