# NewbieAddress

> 地址组件

数据优先级：`dataSource` > `url` > `本地数据`

Version: 1.0.0

## Props

| Prop name    | Description                                                              | Type                  | Values | Default      |
| ------------ | ------------------------------------------------------------------------ | --------------------- | ------ | ------------ |
| value        |                                                                          | string\|number\|array | -      | () =&gt; []  |
| level        | 生成哪个级别的数据<br/> 1: 省, 2: 省市, 3: 省市区                        | number                | -      | 3            |
| disabled     | 是否禁用选择器                                                           | boolean               | -      | false        |
| placeholder  | 占位符                                                                   | string                | -      | "请选择地区" |
| url          | 获取数据的链接，如果不传则使用本地数据                                   | string                | -      | ""           |
| afterFetched | 返回数据处理函数，处理后返回数据格式必需符合下面的 [数据格式](#数据格式) | func                  | -      | null         |
| dataSource   | 自定义数据源                                                             | array                 | -      | []           |

## Events

| Event name   | Properties | Description |
| ------------ | ---------- | ----------- |
| update:value |            |

---

## 数据格式

```json
{
        "value": "440000",
        "alias": "广东省",
        "label": "广东",
        "children": [
	            "value": "440100",
                "alias": "广东省广州市",
                "label": "广州",
                "children": [
                    { "value": "440101", "alias": "广东省广州市", "label": "广州" },
                ]
        ]
}
```

## 示例

---

<script setup>
import { ref } from "vue";
import NewbieAddress from "@components/address/NewbieAddress.jsx";

const content = ref(["440000", "440100", "440103"])
</script>

<p>所选地区结果：{{ `[${content ? content.join(",") : ""}]` }}</p>
<NewbieAddress v-model:value="content" style="width: 300px"></NewbieAddress>

```vue
<script setup>
import { ref } from "vue"
import { NewbieAddress } from "jobsys-newbie"

const content = ref(["440000", "440100", "440103"])
</script>

<template>
	<div>
		<NewbieAddress v-model:value="content" style="width: 300px"></NewbieAddress>
		<p>所选地区结果：{{ `[${content ? content.join(",") : ""}]` }}</p>
	</div>
</template>
```
