# NewbieUploader

> 上传组件

Version: 1.0.0

## Props

| Prop name   | Description                                                                                                                    | Type                  | Values                            | Default        |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------ | --------------------- | --------------------------------- | -------------- |
| value       |                                                                                                                                | object\|array\|string | -                                 | () =&gt; ({})  |
| name        | 上传文件字段名                                                                                                                 | string                | -                                 | "file"         |
| headers     | 设置上传的请求头部，IE10 以上有效                                                                                              | object                | -                                 | {}             |
| accept      | 接受上传的文件类型, 详见 [input accept Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#accept) | string                | -                                 | ""             |
| type        | 上传列表的内建样式，支持三种基本样式 text, picture 和 picture-card                                                             | string                | `text`, `picture`, `picture-card` | "picture-card" |
| disabled    | 是否禁用                                                                                                                       | boolean               | -                                 | false          |
| maxSize     | 单个文件大小上限，单位为 MB                                                                                                    | number                | -                                 | 20             |
| maxNum      | 上传文件个数上限                                                                                                               | number                | -                                 | 1              |
| multiple    | 是否支持多选文件                                                                                                               | boolean               | -                                 | false          |
| multipart   | 是否使用分块上传                                                                                                               | boolean               | -                                 | false          |
| action      | 上传文件的服务器地址                                                                                                           | string                | -                                 | ""             |
| extraData   | 上传时的附加参数                                                                                                               | object                | -                                 | {}             |
| uploadText  | 上传按钮文本                                                                                                                   | string                | -                                 | "上传"         |
| uploadProps | 原生 [Uploader](https://www.antdv.com/components/upload-cn#api) 配置                                                           | object                | -                                 | {}             |

## Events

| Event name   | Properties                      | Description    |
| ------------ | ------------------------------- | -------------- |
| update:value |                                 |
| success      | **fileList** `Array` - 文件列表 | 上传成功时触发 |

---

## 示例

---

<script setup>
import { ref } from "vue";
import NewbieUploader from "@components/uploader/NewbieUploader.jsx";
import NewbieProvider from "@components/provider/NewbieProvider.jsx";

const fileList = ref([
	{
		uid: "-1",
		name: "image.png",
		status: "done",
		url: "https://aliyuncdn.antdv.com/logo.png",
	},
	{
		uid: "-2",
		name: "image.png",
		status: "done",
		url: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png",
	},
	{
		uid: "-3",
		name: "image.png",
		status: "done",
		url: "https://gw.alipayobjects.com/zos/antfincdn/LlvErxo8H9/photo-1503185912284-5271ff81b9a8.webp",
	},
	{
		uid: "-xxx",
		percent: 50,
		name: "image.png",
		status: "uploading",
		url: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png",
	},
]);

const files = ref([]);

const provider = {
	uploader: {
		uploadUrl: "",
		defaultFileItem: {
			id: "id",
			name: "name",
			url: "url",
			path: "path",
			thumbUrl: "thumbUrl",
		},
	},
};
</script>

<NewbieProvider v-bind="provider">
    <p>图片上传</p>
    <NewbieUploader v-model:value="fileList" accept=".png,.jpg,.jpeg" :max-num="5" action="https://www.mocky.io/v2/5cc8019d300000980a055e76"></NewbieUploader>
    <p>文件上传</p>
    <NewbieUploader v-model:value="files" type="file" action="https://www.mocky.io/v2/5cc8019d300000980a055e76"></NewbieUploader>
</NewbieProvider>
