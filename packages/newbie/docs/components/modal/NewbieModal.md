# NewbieModal

> 弹窗组件
> 可以选择使用 Modal 或者是 Drawer
> Modal 支持全屏以及拖动

Version: 1.0.0

## Props

| Prop name   | Description                                                                                                                                   | Type           | Values            | Default |
| ----------- | --------------------------------------------------------------------------------------------------------------------------------------------- | -------------- | ----------------- | ------- |
| type        | 弹窗类型                                                                                                                                      | string         | `modal`, `drawer` | "modal" |
| visible     | 可见性控制<br/>`v-model:visible`                                                                                                              | boolean        | -                 | false   |
| width       | 宽度                                                                                                                                          | number\|string | -                 | 800     |
| height      | 高度，仅当 Type 为 modal 时有效                                                                                                               | number\|string | -                 | 500     |
| title       | 标题                                                                                                                                          | string         | -                 | ""      |
| beforeClose | 为 `Function` 时返回 Promise, resolve(true) 正常关闭，resolve(false) 可阻止关闭；<br/>为 `Object` 时参考 [下方配置 beforeclose](#beforeclose) | func\|object   | -                 | null    |
| modalProps  | 原生 [Modal](https://www.antdv.com/components/modal-cn#api) 或者是 [Drawer](https://www.antdv.com/components/drawer-cn#api) 的配置            | object         | -                 | {}      |

## Events

| Event name     | Properties                       | Description    |
| -------------- | -------------------------------- | -------------- |
| update:visible | **visible** `boolean` - 是否可见 |
| close          |                                  | 关闭弹窗时回调 |

---

## 详细参数

### `beforeClose`

| Prop name  | Description          | Type            | Values | Default |
| ---------- | -------------------- | --------------- | ------ | ------- |
| title      | Modal 提示框的 title | string          | -      | ""      |
| content    | Modal 提示框的内容   | string          | -      | ""      |
| okText     | 确认按钮的文字       | string          | -      | "确定"  |
| cancelText | 取消按钮的文字       | string          | -      | "取消"  |
| trigger    | 是否出现提示框       | func \| boolean | -      | false   |

## 示例

---

<script setup>
import { Button } from "ant-design-vue";
import { ref } from "vue";
import NewbieModal from "@components/modal/NewbieModal.jsx"

const modalVisible = ref(false);
const drawerVisible = ref(false);
</script>

<Button @click="modalVisible = true" type="primary">弹出 Modal</Button>
<NewbieModal v-model:visible="modalVisible" title="这里是标题">这里是内容</NewbieModal>

```vue
<Button @click="modalVisible = true" type="primary">弹出Modal</Button>
<NewbieModal v-model:visible="modalVisible" title="这里是标题">这里是内容</NewbieModal>
```

---

<Button @click="drawerVisible = true" type="primary" style="margin-top: 20px;">弹出 Drawer</Button>
<NewbieModal v-model:visible="drawerVisible" type="drawer" title="这里是标题">这里是内容</NewbieModal>

```vue
<Button @click="drawerVisible = true" type="primary" style="margin-top: 20px;">弹出Drawer</Button>
<NewbieModal v-model:visible="drawerVisible" type="drawer" title="这里是标题">这里是内容</NewbieModal>
```
