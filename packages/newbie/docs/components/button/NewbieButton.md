# NewbieButton

> 按钮组件
> 在原生基础上增加了 loading 状态，以及图标的支持
> loading 状态配合 useFetch 可以自动化处理

Version: 1.0.0

## Props

| Prop name    | Description                                                        | Type    | Values                                                  | Default                     |
| ------------ | ------------------------------------------------------------------ | ------- | ------------------------------------------------------- | --------------------------- |
| type         | 按钮类型                                                           | string  | `primary`, `ghost`, `dashed`, `link`, `text`, `default` | "default"                   |
| size         | 按钮大小                                                           | string  | `large`, `middle`, `small`                              | "middle"                    |
| disabled     | 失效状态                                                           | boolean | -                                                       | false                       |
| danger       | 设置危险按钮                                                       | boolean | -                                                       | false                       |
| label        | 按钮标签                                                           | string  | -                                                       | ""                          |
| icon         | 按钮图标，需要使用                                                 | object  | -                                                       | null                        |
| iconPosition | 按钮图标位置                                                       | string  | `left`, `right`                                         | "left"                      |
| fetcher      | 请求状态控制器<br/>`@params` .loading 是否加载中                   | object  | -                                                       | {<br/> loading: false<br/>} |
| buttonProps  | 原生 [Button](https://www.antdv.com/components/button-cn#api) 配置 | object  | -                                                       | {}                          |

## Events

| Event name | Properties                   | Description |
| ---------- | ---------------------------- | ----------- |
| click      | **event** `Event` - 点击事件 |

---

## 示例

---

<script setup>
import { ref, h } from "vue";
import {Space} from "ant-design-vue";
import {EditOutlined, SaveOutlined} from "@ant-design/icons-vue";
import NewbieButton from "@components/button/NewbieButton.jsx";

const fetcher = ref({loading: false});

const onSave = () => {
    fetcher.value.loading = true;

    setTimeout(() => {
        fetcher.value.loading = false
    }, 2000)
}
</script>

<Space>

<NewbieButton>默认</NewbieButton>
<NewbieButton type="primary">点我</NewbieButton>
<NewbieButton type="primary" size="small">小点</NewbieButton>
<NewbieButton type="primary" size="large">大点</NewbieButton>
<NewbieButton type="primary" danger>危险</NewbieButton>
<NewbieButton type="primary" danger disabled>禁用</NewbieButton>

</Space>

```vue
<NewbieButton>默认</NewbieButton>
<NewbieButton type="primary">点我</NewbieButton>
<NewbieButton type="primary" size="small">点我</NewbieButton>
<NewbieButton type="primary" size="larger">点我</NewbieButton>
<NewbieButton type="primary" danger>危险</NewbieButton>
<NewbieButton type="primary" danger disabled>危险</NewbieButton>
```

---

<Space>
<NewbieButton type="primary" :icon="h(EditOutlined)" label="编辑"></NewbieButton>
<NewbieButton type="primary" :icon="h(EditOutlined)" label="编辑" icon-position="right"></NewbieButton>
</Space>

```vue
<NewbieButton type="primary" :icon="h(EditOutlined)" label="编辑"></NewbieButton>
<NewbieButton type="primary" :icon="h(EditOutlined)" icon-position="right" label="编辑"></NewbieButton>
```

---

<NewbieButton type="primary" :fetcher="fetcher" :icon="h(SaveOutlined)" label="保存" @click="onSave"></NewbieButton>

```vue
<template>
	<NewbieButton type="primary" :fetcher="fetcher" :icon="h(SaveOutlined)" label="保存" @click="onSave"></NewbieButton>
</template>

<script setup>
import { NewbieButton } from "jobsys-newbie"
import { h } from "vue"
import { SaveOutlined } from "@ant-design/icons-vue"

const fetcher = ref({ loading: false })

const onSave = () => {
	fetcher.value.loading = true

	setTimeout(() => {
		fetcher.value.loading = false
	}, 2000)
}
</script>
```
