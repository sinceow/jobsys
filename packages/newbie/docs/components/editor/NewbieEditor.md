# NewbieEditor

> 富文本编辑器
> 基于 WangEditor

Version: 1.0.0

[See](https://www.wangeditor.com/v5/for-frame.html#vue3)

## Props

| Prop name   | Description                                                                                                                                                                              | Type    | Values | Default                                               |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | ------ | ----------------------------------------------------- |
| v-model     | 输入框的值<br/>`@model` true                                                                                                                                                             | string  | -      | ""                                                    |
| placeholder | 占位符                                                                                                                                                                                   | string  | -      | "请输入内容..."                                       |
| disabled    | 是否禁用                                                                                                                                                                                 | boolean | -      | false                                                 |
| readonly    | 是否只读                                                                                                                                                                                 | boolean | -      | false                                                 |
| config      | <br/>`@params` .toolbarConfig [工具栏配置](https://www.wangeditor.com/v5/toolbar-config.html)<br/>`@params` .editorConfig [编辑器配置](https://www.wangeditor.com/v5/editor-config.html) | object  | -      | {<br/> toolbarConfig: {},<br/> editorConfig: {}<br/>} |

## Events

| Event name   | Properties | Description |
| ------------ | ---------- | ----------- |
| update:value |            |

---

## 方法

| 名称       | 参数                     | 说明           |
| ---------- | ------------------------ | -------------- |
| setContent | (content:string) => void | 设置编辑器内容 |

## Provider

| 名称      | 参数   | 说明                        |
| --------- | ------ | --------------------------- |
| uploadUrl | string | 编辑器中的图片,文件上传路径 |

```html
<NewbieProvider :eidtor="{uploadeUrl: 'somewhere-that-can-upload-files'}" />
```

## 示例

---

<script setup>  
import { message, Button } from "ant-design-vue";
import { ref } from "vue";
import NewbieEditor from "@components/editor/NewbieEditor.jsx";

const content = ref('');

const getContent = () => {
    message.success(content.value)
}
</script>

<Button type="primary" @click="getContent">获取内容</Button>

---

<NewbieEditor v-model:value="content" placeholder="来吧，写吧" />

```vue
<template>
	<Button type="primary" @click="getContent">获取内容</Button>
	<NewbieEditor v-model:value="content" placeholder="来吧，写吧" />
</template>

<script setup>
import { NewbieEditor } from "jobsys-newbie"
import { message, Button } from "ant-design-vue"
import { ref } from "vue"

const content = ref("")

const getContent = () => {
	message.success(content.value)
}
</script>
```
