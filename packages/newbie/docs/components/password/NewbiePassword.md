# NewbiePassword

> 密码输入框

分为 4 个密码强度等级，每有以下字符的一种增强一个等级：数字，大写字母，小写字母，特殊字符
每个等级都有对应的颜色，当输入的密码包含对应的等级时，对应的等级颜色会高亮

Version: 1.0.0

## Props

| Prop name | Description | Type           | Values | Default |
| --------- | ----------- | -------------- | ------ | ------- |
| value     | 输入框的值  | string         | -      | ""      |
| min       | 最小长度    | number         | -      | 8       |
| max       | 最大长度    | number         | -      | 20      |
| width     | 宽度        | number\|string | -      | 300     |

## Events

| Event name   | Properties                   | Description            |
| ------------ | ---------------------------- | ---------------------- |
| update:value |                              |
| change       | **event** `Event` - 事件对象 | 输入框内容变化时的回调 |

---

## 示例

---

<script setup>
import { ref } from "vue" ;
import NewbiePassword from "@components/password/NewbiePassword.jsx";

const password = ref("")

</script>

<NewbiePassword  v-model:value="password" width="300"/>

```vue
<script setup>
const password = ref("")
</script>
<NewbiePassword v-model:value="password" width="300" />
```
