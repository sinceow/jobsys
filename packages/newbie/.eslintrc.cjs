module.exports = {
    env: {
        browser: true,
        es2021: true
    },
    globals: {
        defineOptions: true
    },
    extends: ["eslint-config-prettier", "plugin:import/recommended", "eslint:recommended", "plugin:vue/vue3-recommended", "plugin:prettier/recommended"],
    overrides: [],
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
        ecmaFeatures: {
            jsx: true
        }
    },
    plugins: ["vue"],
    rules: {
        "import/prefer-default-export": "off",
        "import/no-extraneous-dependencies": "off",
        "import/no-duplicates": "off",
        "import/named": "off",
        "import/order": "off",
        "import/first": "off",
        "vue/attributes-order": "off",
        "vue/no-mutating-props": "off",
        "vue/no-reserved-component-names": "off",
        "vue/no-v-html": "off",
        "no-param-reassign": "off",
        "no-console": "off",
        "no-restricted-globals": "off",
        "no-debugger": "off",
        "no-use-before-define": "off",
        "no-promise-executor-return": "off",
        "spaced-comment": "off"
    }
};