import _ExSurvey from "./ExSurvey.jsx"
import withInstall from "../../utils/withInstall"

export const ExSurvey = withInstall(_ExSurvey)
export default ExSurvey
