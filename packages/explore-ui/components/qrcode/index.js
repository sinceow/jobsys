import _ExQrcode from "./ExQrcode.jsx"
import withInstall from "../../utils/withInstall"

export const ExQrcode = withInstall(_ExQrcode)
export default ExQrcode
