import _ExButton from "./ExButton.jsx"
import withInstall from "../../utils/withInstall"
export const ExButton = withInstall(_ExButton)
export default ExButton
