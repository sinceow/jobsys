import _ExResult from "./ExResult.jsx"
import withInstall from "../../utils/withInstall"

export const ExResult = withInstall(_ExResult)
export default ExResult
