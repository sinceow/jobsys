import _ExProvider from "./ExProvider.jsx"
import withInstall from "../../utils/withInstall"
export const ExProvider = withInstall(_ExProvider)
export default ExProvider
