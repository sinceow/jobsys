import _ExDecorator from "./ExDecorator.jsx"
import withInstall from "../../utils/withInstall"

export const ExDecorator = withInstall(_ExDecorator)
export default ExDecorator
