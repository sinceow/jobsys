import _ExTheme from "./ExTheme.jsx"
import withInstall from "../../utils/withInstall"
export const ExTheme = withInstall(_ExTheme)
export default ExTheme
