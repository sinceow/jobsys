import { defineComponent } from "vue"
import "./index.less"

/**
 * 主题组件
 * @version 1.0.0
 */
export default defineComponent({
	name: "ExTheme",
})
