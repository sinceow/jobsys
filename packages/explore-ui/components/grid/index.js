import _ExGrid from "./ExGrid.jsx"
import withInstall from "../../utils/withInstall"
export const ExGrid = withInstall(_ExGrid)
export default ExGrid
