import _ExSector from "./ExSector.jsx"
import withInstall from "../../utils/withInstall"

export const ExSector = withInstall(_ExSector)
export default ExSector
