import _ExPagination from "./ExPagination.jsx"
import withInstall from "../../utils/withInstall"

export const ExPagination = withInstall(_ExPagination)
export default ExPagination
