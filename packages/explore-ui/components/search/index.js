import _ExSearch from "./ExSearch.jsx"
import withInstall from "../../utils/withInstall"

export const ExSearch = withInstall(_ExSearch)
export default ExSearch
