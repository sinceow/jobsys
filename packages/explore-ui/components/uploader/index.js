import _ExUploader from "./ExUploader.jsx"
import withInstall from "../../utils/withInstall"

export const ExUploader = withInstall(_ExUploader)
export default ExUploader
