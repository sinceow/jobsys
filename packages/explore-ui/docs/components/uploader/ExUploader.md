# ExUploader

> ExUploader 文件上传

Version: 1.0.0

## Props

| Prop name     | Description                                                                                                                    | Type          | Values            | Default       |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------ | ------------- | ----------------- | ------------- |
| modelValue    |                                                                                                                                | object\|array | -                 | () =&gt; ({}) |
| name          | 上传文件字段名                                                                                                                 | string        | -                 | "file"        |
| accept        | 接受上传的文件类型, 详见 [input accept Attribute](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#accept) | string        | -                 | ""            |
| headers       | 设置上传的请求头部，IE10 以上有效                                                                                              | object        | -                 | {}            |
| type          | 上传列表的基本样式 text, picture                                                                                               | string        | `text`, `picture` | "picture"     |
| disabled      | 是否禁用                                                                                                                       | boolean       | -                 | false         |
| readonly      | 是否只读                                                                                                                       | boolean       | -                 | false         |
| maxSize       | 单个文件大小上限，单位为 MB                                                                                                    | number        | -                 | 10            |
| maxNum        | 上传文件个数上限                                                                                                               | number        | -                 | 1             |
| multiple      | 是否支持多选文件                                                                                                               | boolean       | -                 | false         |
| action        | 上传文件的服务器地址                                                                                                           | string        | -                 | ""            |
| afterUpload   | 上传后的数据处理回调                                                                                                           | func          | -                 | null          |
| extraData     | 上传时的附加参数                                                                                                               | object        | -                 | {}            |
| uploadText    | 上传按钮文本                                                                                                                   | string        | -                 | "上传"        |
| uploadIcon    | 上传区域图标名称或图片链接，等同于 Icon 组件的 name 属性                                                                       | string        | -                 | "photograph"  |
| uploaderProps | [原生配置](https://vant-contrib.gitee.io/vant/#/zh-CN/uploader)                                                                | object        | -                 | {}            |

## Events

| Event name        | Properties | Description |
| ----------------- | ---------- | ----------- |
| update:modelValue |            |
| success           |            |
| change            |            |

---
