# ExButton

> 主题组件

Version: 1.0.0

## Props

| Prop name    | Description                                                   | Type    | Values                                    | Default                     |
| ------------ | ------------------------------------------------------------- | ------- | ----------------------------------------- | --------------------------- |
| type         | 按钮类型                                                      | string  | `primary`, `success`, `warning`, `danger` | "default"                   |
| size         | 按钮大小                                                      | string  | `large`, `normal`, `small`, `mini`        | "normal"                    |
| disabled     | 失效状态                                                      | boolean | -                                         | false                       |
| round        | 是否圆形按钮                                                  | boolean | -                                         | true                        |
| block        | 是否块状按钮                                                  | boolean | -                                         | true                        |
| text         | 按钮标签                                                      | string  | -                                         | ""                          |
| icon         | 按钮图标，需要使用                                            | object  | -                                         | null                        |
| iconPosition | 按钮图标位置                                                  | string  | `left`, `right`                           | "left"                      |
| fetcher      | 请求状态控制器<br/>`@params` .loading 是否加载中              | object  | -                                         | {<br/> loading: false<br/>} |
| buttonProps  | [原生配置](https://vant-contrib.gitee.io/vant/#/zh-CN/button) | object  | -                                         | {}                          |

## Events

| Event name | Properties                   | Description |
| ---------- | ---------------------------- | ----------- |
| click      | **event** `Event` - 点击事件 |

---

## 这里加点内容
