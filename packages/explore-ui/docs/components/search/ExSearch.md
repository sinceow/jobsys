# ExSearch

> ExSearch 搜索组件

Version: 1.0.0

## Props

| Prop name   | Description                                                       | Type    | Values                    | Default            |
| ----------- | ----------------------------------------------------------------- | ------- | ------------------------- | ------------------ |
| modelValue  |                                                                   | string  | -                         | ""                 |
| columns     | 搜索项的配置, 详见 [ExSearchItemConfig](#exsearchitemconfig-配置) | array   | -                         | []                 |
| placeholder | 占位提示文字                                                      | string  | -                         | "请输入搜索关键词" |
| keyword     | 名称，作为提交表单时的标识符                                      | string  | -                         | "keyword"          |
| label       | 搜索框左侧文本                                                    | string  | -                         | ""                 |
| readonly    | 是否将输入框设为只读状态，只读状态下无法输入内容                  | boolean | -                         | false              |
| disabled    | 是否禁用输入框                                                    | boolean | -                         | false              |
| inputAlign  | 输入框内容对齐方式                                                | string  | `left`, `right`, `center` | "left"             |
| searchProps | [原生配置](https://vant-contrib.gitee.io/vant/#/zh-CN/search)     | object  | -                         | {}                 |

## Events

| Event name        | Properties | Description |
| ----------------- | ---------- | ----------- |
| update:modelValue |            |
| search            |            |

---
