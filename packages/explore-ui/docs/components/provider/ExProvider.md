# ExProvider

> Explore 配置组件

Version: 1.0.0

## Props

| Prop name  | Description                                                                                                                                                                                                                                                                                                                      | Type   | Values | Default                                                                                                                                                          |
| ---------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------ | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| uploader   | <br/>`@typedef` {Object} UploaderProviderProps `ExUploader` 配置<br/>`@typedef` {Object} UploadFileItem 文件项<br/>`@property` 上传地址<br/>`@property` 默认文件项<br/>`@property` [id] 文件 ID<br/>`@property` [name] 文件名<br/>`@property` .url 文件地址<br/>`@property` .path 文件路径<br/>`@property` [thumbUrl] 缩略图地址 | object | -      | {<br/> uploadUrl: "",<br/><br/> defaultFileItem: {<br/> id: "id",<br/> name: "name",<br/> url: "url",<br/> path: "path",<br/> thumbUrl: "thumbUrl",<br/> }<br/>} |
| form       | <br/>`@typedef` {Object} FormProviderProps `ExForm` 配置<br/>`@property` [format] 格式化配置, 如 {date: true} 表示在提交表单时使用 `useFormFormat` 格式所有日期字段<br/>`@property` [afterFetched] 处理接口返回数据的函数                                                                                                        | object | -      | {<br/> format: {},<br/> afterFetched: null<br/>}                                                                                                                 |
| search     | <br/>`@typedef` {Object} SearchProviderProps `ExSearch` 配置<br/>`@property` [maskClass] 定制伪 Input 样式<br/>`@property` [inputClass] 弹层中 Input 的样式                                                                                                                                                                      | object | -      | {<br/> maskClass: "",<br/> inputClass: ""<br/>}                                                                                                                  |
| pagination | <br/>`@typedef` {Object} PaginationProviderProps `ExPagination` 配置<br/>`@typedef` {Object} PaginationRequestKeys 请求参数键名<br/>`@property` [afterFetched] 处理接口返回数据的函数<br/>`@property` [requestKeys] 请求参数键名<br/>`@property` [currentPage] 当前页码<br/>`@property` [pageSize] 每页条数                      | object | -      | {<br/> afterFetched: null,<br/><br/> requestKeys: {<br/> currentPage: "currentPage",<br/> pageSize: "pageSize",<br/> }<br/>}                                     |

---
