# ExForm

> ExForm 表单

Version: 1.0.0

## Props

| Prop name         | Description                                                                                                                                                                                                                     | Type                   | Values | Default |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | ------ | ------- |
| title             | 表单标题                                                                                                                                                                                                                        | string                 | -      | ""      |
| labelWidth        | 表单项 label 宽度，默认单位为 px                                                                                                                                                                                                | string\|number         | -      | "6.2em" |
| colon             | 是否在 label 后面添加冒号                                                                                                                                                                                                       | boolean                | -      | false   |
| inset             | 是否使用卡片模式                                                                                                                                                                                                                | boolean                | -      | false   |
| data              | 表单数据，用于初始化表单，并会进行 Watch                                                                                                                                                                                        | object\|string         | -      | ""      |
| autoLoad          | 是否自动加载<br/>true: 表示自动加载数据<br/>Array,String: 表示会对 `extraData` 数据中的相关字段进行非空验证，不为空再加载数据                                                                                                   | boolean\|array\|string | -      | true    |
| fetchUrl          | 获取表单数据的 URL                                                                                                                                                                                                              | string                 | -      | ""      |
| extraData         | 额外的数据，在提交时会合并到表单数据中并一起提交                                                                                                                                                                                | object                 | -      | {}      |
| disabled          | 是否禁用表单                                                                                                                                                                                                                    | boolean                | -      | false   |
| readonly          | 是否只读                                                                                                                                                                                                                        | boolean                | -      | false   |
| fixed             | submit button 是否固定在底部                                                                                                                                                                                                    | boolean                | -      | false   |
| submitUrl         | 提交数据 URL                                                                                                                                                                                                                    | string                 | -      | ""      |
| submitButtonText  | 提交按钮文字                                                                                                                                                                                                                    | string                 | -      | "保存"  |
| submitConfirmText | 提交确认提示内容                                                                                                                                                                                                                | string                 | -      | ""      |
| submitDisabled    | 是否禁用提交按钮                                                                                                                                                                                                                | boolean                | -      | false   |
| dividerProps      | 当有分割线时，分割线的配置                                                                                                                                                                                                      | object                 | -      | {}      |
| form              | 表单配置，[见下表](#form-表单配置)                                                                                                                                                                                              | array                  | -      | []      |
| afterFetched      | fetch 返回数据处理函数<br/>`@return` {Object} 返回处理后的数据，将用于初始化表单                                                                                                                                                | func                   | -      | null    |
| beforeSubmit      | <br/>`@typedef` {Object} ExposedFormData<br/>`@property` Format 后的表单数据<br/>`@property` 原生的表单数据<br/>`@params` undefined<br/>`@return` {Boolean\|Object} return false 会阻止提交操作，return Object 会替换提交的数据 | func                   | -      | null    |
| afterSubmit       | 提交成功后的回调                                                                                                                                                                                                                | func                   | -      | null    |
| formProps         | [原生配置](https://vant-contrib.gitee.io/vant/#/zh-CN/form)                                                                                                                                                                     | object                 | -      | {}      |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| success    |            |

---
