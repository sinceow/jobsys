# ExDecorator

> 装饰组件

Version: 1.0.0

## Props

| Prop name | Description | Type           | Values                             | Default |
| --------- | ----------- | -------------- | ---------------------------------- | ------- |
| color     | 自定义颜色  | string         | `green`, `blue`, `black`, `orange` | "green" |
| height    | 高度        | number\|string | -                                  | "auto"  |

---
