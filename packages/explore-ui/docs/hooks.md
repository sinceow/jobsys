## Constants

<dl>
<dt><a href="#STATUS">STATUS</a> : <code>Object</code></dt>
<dd><p>请求返回状态码</p>
</dd>
<dt><a href="#usePage">usePage</a></dt>
<dd><p>分页请求</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#useSm2">useSm2()</a></dt>
<dd><p>国密 sm2</p>
</dd>
<dt><a href="#useSm3">useSm3(msg, [options])</a> ⇒ <code>*</code></dt>
<dd><p>国密 sm3 加密</p>
</dd>
<dt><a href="#useSm4Encrypt">useSm4Encrypt(msg, key, [options])</a> ⇒ <code>Uint8Array</code></dt>
<dd><p>国密 sm4 加密</p>
</dd>
<dt><a href="#useSm4Decrypt">useSm4Decrypt(encryptData, key, [options])</a> ⇒ <code>Uint8Array</code></dt>
<dd><p>国密 sm4 解密</p>
</dd>
<dt><a href="#useDayjs">useDayjs(date, [format])</a> ⇒ <code>dayjs.Dayjs</code></dt>
<dd><p>根据格式创建 Dayjs 对象</p>
</dd>
<dt><a href="#useDateFormat">useDateFormat(date, format)</a> ⇒ <code>string</code></dt>
<dd><p>格式化日期</p>
</dd>
<dt><a href="#useDateUnix">useDateUnix(date)</a> ⇒ <code>number</code> | <code>string</code></dt>
<dd><p>获取日期的 Unix 时间戳</p>
</dd>
<dt><a href="#useTimestampFormat">useTimestampFormat(timestamp, [format])</a> ⇒ <code>string</code></dt>
<dd><p>根据时间戳格式化日期</p>
</dd>
<dt><a href="#useHiddenForm">useHiddenForm(options)</a> ⇒ <code>HTMLFormElement</code></dt>
<dd><p>创建一个隐藏的表单</p>
</dd>
<dt><a href="#useProcessStatus">useProcessStatus(res, ops)</a></dt>
<dd><p>处理请求结果</p>
</dd>
<dt><a href="#useProcessStatusSuccess">useProcessStatusSuccess(res, success)</a></dt>
<dd><p>处理正确请求结果</p>
</dd>
<dt><a href="#useFormFail">useFormFail(e)</a></dt>
<dd><p>处理表单提交失败</p>
</dd>
<dt><a href="#useFormFormat">useFormFormat(form, [format])</a> ⇒ <code>Object</code></dt>
<dd><p>处理表单数据</p>
</dd>
<dt><a href="#useFetch">useFetch([fetcher])</a> ⇒ <code>Object</code></dt>
<dd><p>通用 AJAX 请求</p>
</dd>
<dt><a href="#get">get(url, [config])</a> ⇒ <code>Promise.&lt;unknown&gt;</code></dt>
<dd><p>get请求</p>
</dd>
<dt><a href="#post">post(url, data, [config])</a> ⇒ <code>Promise.&lt;unknown&gt;</code></dt>
<dd><p>post请求</p>
</dd>
<dt><a href="#useTextFromOptionsValue">useTextFromOptionsValue(value, options)</a> ⇒ <code>*</code> | <code>string</code> | <code>string</code></dt>
<dd><p>从 options 中根据 value 获取 text</p>
</dd>
<dt><a href="#useFindTextsInValues">useFindTextsInValues(options, values, [adapter])</a> ⇒</dt>
<dd><p>从嵌套的 options 中根据 value 获取 text, 如 [1, 3] =&gt; [&quot;东&quot;, &quot;南&quot;]</p>
</dd>
<dt><a href="#useFindLabelsFromPath">useFindLabelsFromPath(options, path, [adapter])</a> ⇒</dt>
<dd><p>从嵌套的 options 中根据 value 获取 label</p>
</dd>
<dt><a href="#useFindTextsFromPath">useFindTextsFromPath(options, path, [adapter])</a> ⇒</dt>
<dd><p>[移动端适配]
从嵌套的 options 中根据 value 获取 text</p>
</dd>
<dt><a href="#useFindParentValues">useFindParentValues(options, value, adapter)</a> ⇒</dt>
<dd><p>从嵌套的 options 中根据 value 获取整个 values 路径, 如地区路径: 440113 =&gt; [440000, 440100, 440113]</p>
</dd>
</dl>

<a name="STATUS"></a>

## STATUS : <code>Object</code>
请求返回状态码

**Kind**: global constant  
<a name="usePage"></a>

## usePage
分页请求

**Kind**: global constant  

| Param | Type | Description |
| --- | --- | --- |
| pagination | <code>object</code> | 分页对象 |
| pagination.uri | <code>string</code> | 请求地址 |
| [pagination.params] | <code>object</code> | 请求参数 |
| [pagination.page] | <code>number</code> | 当前页码 |
| [pagination.finishedText] | <code>string</code> | 加载完毕文本 |
| [pagination.loading] | <code>boolean</code> | 加载状态 |
| [pagination.finished] | <code>boolean</code> | 加载完毕 |
| [pagination.error] | <code>boolean</code> | 是否错误 |
| [pagination.errorText] | <code>string</code> | 错误文本 |
| [pagination.empty] | <code>boolean</code> | 是否无内容 |
| [pagination.items] | <code>array</code> | 分页数据 |
| refresh | <code>boolean</code> | 是否刷新 |
| [process] | <code>function</code> | 数据处理函数 |

<a name="useSm2"></a>

## useSm2()
国密 sm2

**Kind**: global function  
<a name="useSm3"></a>

## useSm3(msg, [options]) ⇒ <code>\*</code>
国密 sm3 加密

**Kind**: global function  

| Param | Type |
| --- | --- |
| msg |  | 
| [options] | <code>Object</code> | 

<a name="useSm4Encrypt"></a>

## useSm4Encrypt(msg, key, [options]) ⇒ <code>Uint8Array</code>
国密 sm4 加密

**Kind**: global function  

| Param | Type |
| --- | --- |
| msg |  | 
| key |  | 
| [options] | <code>Object</code> | 

<a name="useSm4Decrypt"></a>

## useSm4Decrypt(encryptData, key, [options]) ⇒ <code>Uint8Array</code>
国密 sm4 解密

**Kind**: global function  

| Param | Type |
| --- | --- |
| encryptData |  | 
| key |  | 
| [options] | <code>Object</code> | 

<a name="useDayjs"></a>

## useDayjs(date, [format]) ⇒ <code>dayjs.Dayjs</code>
根据格式创建 Dayjs 对象

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| date | <code>string</code> \| <code>number</code> |  |
| [format] | <code>string</code> | 日期格式 |

<a name="useDateFormat"></a>

## useDateFormat(date, format) ⇒ <code>string</code>
格式化日期

**Kind**: global function  

| Param | Type |
| --- | --- |
| date | <code>dayjs.Dayjs</code> \| <code>Date</code> | 
| format | <code>string</code> | 

<a name="useDateUnix"></a>

## useDateUnix(date) ⇒ <code>number</code> \| <code>string</code>
获取日期的 Unix 时间戳

**Kind**: global function  

| Param | Type |
| --- | --- |
| date | <code>dayjs.Dayjs</code> \| <code>Date</code> | 

<a name="useTimestampFormat"></a>

## useTimestampFormat(timestamp, [format]) ⇒ <code>string</code>
根据时间戳格式化日期

**Kind**: global function  

| Param | Description |
| --- | --- |
| timestamp | 时间戳 |
| [format] | 日期格式 |

<a name="useHiddenForm"></a>

## useHiddenForm(options) ⇒ <code>HTMLFormElement</code>
创建一个隐藏的表单

**Kind**: global function  

| Param | Type |
| --- | --- |
| options | <code>Object</code> | 
| options.url | <code>string</code> | 
| options.data | <code>Object</code> | 
| [options.method] | <code>string</code> | 
| options.csrfToken | <code>string</code> | 

<a name="useProcessStatus"></a>

## useProcessStatus(res, ops)
处理请求结果

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| res | <code>object</code> | 请求结果 |
| res.status | <code>string</code> | 请求结果状态 |
| res.result | <code>\*</code> | 请求结果信息 |
| ops | <code>Object.&lt;string, (string\|function())&gt;</code> | 状态的处理对象 |

<a name="useProcessStatusSuccess"></a>

## useProcessStatusSuccess(res, success)
处理正确请求结果

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| res | <code>object</code> | 请求结果 |
| res.status | <code>string</code> | 请求结果状态 |
| res.result | <code>\*</code> | 请求结果信息 |
| success | <code>string</code> \| <code>function</code> | 状态的处理对象 |

<a name="useFormFail"></a>

## useFormFail(e)
处理表单提交失败

**Kind**: global function  

| Param | Type |
| --- | --- |
| e | <code>\*</code> | 

<a name="useFormFormat"></a>

## useFormFormat(form, [format]) ⇒ <code>Object</code>
处理表单数据

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| form | <code>Object</code> |  |
| [format] | <code>Object</code> | 需要处理的类型 |
| [format.date] | <code>boolean</code> \| <code>string</code> \| <code>function</code> | `true`: 转成时间戳，`string`: 为 Format 格式, 如 `"YYYY-MM-DD"`, `function`: 自定义处理函数, 参数为 dayjs 对象 |
| [format.boolean] | <code>boolean</code> | 布尔值处理, 如果开启则 `true` 转成 1, `false` 转成 0 |
| [format.attachment] | <code>string</code> \| <code>function</code> | `string`: 附件字段名, `function`: 自定义处理函数, 参数为附件对象 |

<a name="useFetch"></a>

## useFetch([fetcher]) ⇒ <code>Object</code>
通用 AJAX 请求

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| [fetcher] | <code>Object</code> | 用于存储请求状态的对象 |

<a name="get"></a>

## get(url, [config]) ⇒ <code>Promise.&lt;unknown&gt;</code>
get请求

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| url |  |  |
| [config] | <code>Object</code> | axios config |

<a name="post"></a>

## post(url, data, [config]) ⇒ <code>Promise.&lt;unknown&gt;</code>
post请求

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> |  |
| data | <code>Object</code> |  |
| [config] | <code>Object</code> | axios config |

<a name="useTextFromOptionsValue"></a>

## useTextFromOptionsValue(value, options) ⇒ <code>\*</code> \| <code>string</code> \| <code>string</code>
从 options 中根据 value 获取 text

**Kind**: global function  

| Param |
| --- |
| value | 
| options | 

<a name="useFindTextsInValues"></a>

## useFindTextsInValues(options, values, [adapter]) ⇒
从嵌套的 options 中根据 value 获取 text, 如 [1, 3] => ["东", "南"]

**Kind**: global function  
**Returns**: Array  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| options | <code>Array</code> |  | 嵌套的选项 |
| values | <code>Array</code> |  | 需要查找的值 |
| [adapter] | <code>Object</code> | <code>{value: &quot;value&quot;,label: &quot;label&quot;,children: &quot;children&quot;}</code> | 选项适配器 |

<a name="useFindLabelsFromPath"></a>

## useFindLabelsFromPath(options, path, [adapter]) ⇒
从嵌套的 options 中根据 value 获取 label

**Kind**: global function  
**Returns**: Array  

| Param | Type |
| --- | --- |
| options | <code>Array</code> | 
| path | <code>Array</code> | 
| [adapter] | <code>Object</code> | 

<a name="useFindTextsFromPath"></a>

## useFindTextsFromPath(options, path, [adapter]) ⇒
[移动端适配]从嵌套的 options 中根据 value 获取 text

**Kind**: global function  
**Returns**: Array  

| Param | Type |
| --- | --- |
| options | <code>Array</code> | 
| path | <code>Array</code> | 
| [adapter] | <code>Object</code> | 

<a name="useFindParentValues"></a>

## useFindParentValues(options, value, adapter) ⇒
从嵌套的 options 中根据 value 获取整个 values 路径, 如地区路径: 440113 => [440000, 440100, 440113]

**Kind**: global function  
**Returns**: Array  

| Param |
| --- |
| options | 
| value | 
| adapter | 

