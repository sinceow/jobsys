# Explore-UI

基于 Vant Vue3 的增强组件

## 安装

使用 `npm`

```bash
npm i "jobsys-explore"
```

使用 `yarn`

```bash
yarn i "jobsys-explore"
```

使用 `pnpm`

```bash
pnpm i "jobsys-explore"
```

> 由于 `explore-ui` 使用了 `rem` 作为单位以适应不同的屏幕尺寸，所以需要在 `Vite` 里进行适配

```bash
pnpm i -D "postcss-pxtorem"
```

```js
//vite.config.js

import pixelToRem from "postcss-pxtorem"

export default defineConfig({
    css: {
		postcss: {
			plugins: [
				pixelToRem({
					rootValue({file}) {
					    //在这里可以根据 file 路径进行判断与定制，默认的 rootValue 是 37.5
						return 37.5
					},
					propList: ["*"],
				}),
			],
		},
	},
})
```

## 示例

引入样式

```js
//app.js
import "jobsys-explore/dist/style.css"
```

```js
//app.js
import { createApp } from "vue"
import Explore from "jobsys-explore"

const app = createApp({
    setup: () => {}
})

app.use(Explore).mount("#app")
```

```vue
<template>
   <ExButton type="primary">点兵</ExButton>
</template>
<script setup>
    import { ref } from "vue"
    
    const password = ref("")
</script>
```