import { defineAsyncComponent } from 'vue'

export const pagesComponents = {
  // path: /hooks.html
  "v-456c2ee2": defineAsyncComponent(() => import(/* webpackChunkName: "v-456c2ee2" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/hooks.html.vue")),
  // path: /
  "v-8daa1a0e": defineAsyncComponent(() => import(/* webpackChunkName: "v-8daa1a0e" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/index.html.vue")),
  // path: /components/button/ExButton.html
  "v-fa9c6d92": defineAsyncComponent(() => import(/* webpackChunkName: "v-fa9c6d92" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/components/button/ExButton.html.vue")),
  // path: /components/decorator/ExDecorator.html
  "v-3cd00876": defineAsyncComponent(() => import(/* webpackChunkName: "v-3cd00876" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/components/decorator/ExDecorator.html.vue")),
  // path: /components/provider/ExProvider.html
  "v-47132697": defineAsyncComponent(() => import(/* webpackChunkName: "v-47132697" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/components/provider/ExProvider.html.vue")),
  // path: /components/form/ExForm.html
  "v-40e16512": defineAsyncComponent(() => import(/* webpackChunkName: "v-40e16512" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/components/form/ExForm.html.vue")),
  // path: /components/sector/ExSector.html
  "v-30695eb7": defineAsyncComponent(() => import(/* webpackChunkName: "v-30695eb7" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/components/sector/ExSector.html.vue")),
  // path: /components/search/ExSearch.html
  "v-6c62dc77": defineAsyncComponent(() => import(/* webpackChunkName: "v-6c62dc77" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/components/search/ExSearch.html.vue")),
  // path: /components/uploader/ExUploader.html
  "v-004152b7": defineAsyncComponent(() => import(/* webpackChunkName: "v-004152b7" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/components/uploader/ExUploader.html.vue")),
  // path: /404.html
  "v-3706649a": defineAsyncComponent(() => import(/* webpackChunkName: "v-3706649a" */"D:/workspace/jcomponent/packages/explore-ui/docs/.vuepress/.temp/pages/404.html.vue")),
}
