import { defaultTheme, defineUserConfig, viteBundler } from "vuepress"
import vueJsx from "@vitejs/plugin-vue-jsx"
import { resolve } from "path"
import { globbySync } from "globby"
import pixelToRem from "postcss-pxtorem"

const sidebarComponents = globbySync("components/**/*.md", { cwd: resolve(__dirname, "../") })

export default defineUserConfig({
	base: "/docs/explore-ui/",
	lang: "zh-CN",
	title: "Explore Component",
	description: "Enhanced Vue components based on Vant.",
	bundler: viteBundler({
		viteOptions: {
			plugins: [vueJsx()],
			resolve: {
				alias: {
					"@components": resolve(__dirname, "../../components"),
				},
			},
			css: {
				postcss: {
					plugins: [
						pixelToRem({
							rootValue(file) {
								if (file.file.includes("node_modules/@vuepress")) {
									return 16
								}
								return 37.5
							},
							propList: ["*"],
						}),
					],
				},
			},
		},
	}),
	theme: defaultTheme({
		displayAllHeaders: true,
		navbar: [
			{ text: "首页", link: "/" },
			{
				text: "组件",
				children: sidebarComponents.map((comp) => {
					return {
						text: comp
							.replace(/^components\//, "")
							.replace(/\.md$/, "")
							.split("/")[1],
						link: "/" + comp.replace(/\.md$/, ""),
					}
				}),
			},
			{
				text: "组合式 API",
				link: "/hooks",
			},
		],
	}),
})
