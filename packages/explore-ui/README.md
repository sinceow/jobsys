# TODOs

---

## 增强

-   [ ] ExploreForm

    -   [x] Address
    -   [x] Cascader
    -   [x] Checkbox
    -   [x] Date
    -   [ ] Group
    -   [ ] Html
    -   [x] Input
    -   [x] Number
    -   [x] Radio
    -   [ ] Remote
    -   [x] Select
    -   [x] Switch
    -   [] Tag
    -   [x] Text
    -   [x] Time
    -   [ ] TreeSelect
    -   [x] Uploader

-   [x] ExploreSearch

    -   [x] 同上

-   [x] Field
    -   [x] Disabled
    -   [x] Help

## 新增

-   [ ] 样式定制
-   [ ] 浮动按钮
-   [ ] 列表组件
-   [ ] 背景生成
-   [ ] 简易图表
