# jobsys-explore

## 4.7.0

### Minor Changes

- Add survey component

### Patch Changes

- Fix
- Build
- Drop the unused property
- Add matrix component and closable option
- Fix clear error in ExSearch
- Rewrite ExPagiantion
- Add init and beforeSubmit
- Fix
- Add disabled and readonly for ex form
- hide upload when readonly or disabled
- Add form item pattern support
- Add cascader search and filter search issue
- Fix sector style
- Fix exupload
- Add ex-decorator
- clone the options before traverse
- Upgrade useFormFormat
- Add \_type for uploader
- upgrade
- Fix
- Add useFindParentValues
- Fix form submit await
- Add status adapter and useFetch loading
- Adjust form item render order
- Optimize cache
- Add pagination expose
- Add pagination persistence
- Fix upload file
- Add useFindPropertyRecursive
- upgrade
- Fix beforeSubmit

## 4.6.4

### Patch Changes

- A
- Add useFindOptionByValue
- Upgrade
- Adjust vue version
- Downgrade vue
- AA
- Add fieldNames support for options

## 4.6.3

### Patch Changes

- Add useFindOptionByValue
- Upgrade
- Adjust vue version
- Downgrade vue
- AA
- Add fieldNames support for options

## 4.6.2

### Patch Changes

- Add useFindOptionByValue
- Upgrade
- Adjust vue version
- Downgrade vue
- Add fieldNames support for options

## 4.5.5

### Patch Changes

- Add useFindOptionByValue
- Upgrade
- Downgrade vue
- Add fieldNames support for options

## 4.5.4

### Patch Changes

- Add useFindOptionByValue
- Upgrade
- Downgrade vue
- Add fieldNames support for options

## 4.6.0

### Minor Changes

- Add survey component

### Patch Changes

- Fix
- Build
- Drop the unused property
- Add matrix component and closable option
- Fix clear error in ExSearch
- Rewrite ExPagiantion
- Add init and beforeSubmit
- Fix
- Add disabled and readonly for ex form
- hide upload when readonly or disabled
- Add form item pattern support
- Add cascader search and filter search issue
- Fix sector style
- Fix exupload
- Add ex-decorator
- clone the options before traverse
- Upgrade useFormFormat
- Add \_type for uploader
- upgrade
- Fix
- Add useFindParentValues
- Fix form submit await
- Add status adapter and useFetch loading
- Adjust form item render order
- Add pagination expose
- Fix upload file
- Add useFindPropertyRecursive
- upgrade
- Fix beforeSubmit

## 4.5.2

### Patch Changes

- modify submit form in beforeSubit

## 4.5.1

### Patch Changes

- modify submit form in beforeSubit

## 4.5.0

### Minor Changes

- Add survey component

### Patch Changes

- Fix
- Build
- Drop the unused property
- Add matrix component and closable option
- Fix clear error in ExSearch
- Rewrite ExPagiantion
- Add init and beforeSubmit
- Fix
- Add disabled and readonly for ex form
- hide upload when readonly or disabled
- Add form item pattern support
- Add cascader search and filter search issue
- Fix sector style
- Fix exupload
- Add ex-decorator
- clone the options before traverse
- Upgrade useFormFormat
- Add \_type for uploader
- upgrade
- Fix
- Add useFindParentValues
- Fix form submit await
- Add status adapter and useFetch loading
- Adjust form item render order
- Add pagination expose
- Fix upload file
- Add useFindPropertyRecursive
- upgrade
- Fix beforeSubmit

## 4.4.2

### Patch Changes

- add version
- add search beforeSubmit method

## 4.4.1

### Patch Changes

- add search beforeSubmit method

## 4.4.0

### Minor Changes

- Add survey component

### Patch Changes

- Fix
- Build
- Drop the unused property
- Add matrix component and closable option
- Fix clear error in ExSearch
- Rewrite ExPagiantion
- Add init and beforeSubmit
- Fix
- Add disabled and readonly for ex form
- hide upload when readonly or disabled
- Add form item pattern support
- Add cascader search and filter search issue
- Fix sector style
- Fix exupload
- Add ex-decorator
- clone the options before traverse
- Upgrade useFormFormat
- upgrade
- Fix
- Add useFindParentValues
- Fix form submit await
- Add status adapter and useFetch loading
- Adjust form item render order
- Add pagination expose
- Fix upload file
- Add useFindPropertyRecursive
- upgrade
- Fix beforeSubmit

## 4.3.1

### Patch Changes

- modify submit form in beforeSubit

## 4.2.20

### Patch Changes

- Fix
- Build
- Drop the unused property
- Add matrix component and closable option
- Fix clear error in ExSearch
- Rewrite ExPagiantion
- Add init and beforeSubmit
- Fix
- Add disabled and readonly for ex form
- hide upload when readonly or disabled
- Add form item pattern support
- Add cascader search and filter search issue
- Fix sector style
- Fix exupload
- Add ex-decorator
- clone the options before traverse
- Upgrade useFormFormat
- upgrade
- Fix
- Add useFindParentValues
- Fix form submit await
- Add status adapter and useFetch loading
- Adjust form item render order
- Add pagination expose
- Fix upload file
- Add useFindPropertyRecursive
- upgrade
- Fix beforeSubmit

## 4.2.19

### Patch Changes

- Fix
- Build
- Drop the unused property
- Fix clear error in ExSearch
- Rewrite ExPagiantion
- Add init and beforeSubmit
- Fix
- Add disabled and readonly for ex form
- hide upload when readonly or disabled
- Add form item pattern support
- Add cascader search and filter search issue
- Fix sector style
- Fix exupload
- Add ex-decorator
- clone the options before traverse
- Upgrade useFormFormat
- upgrade
- Fix
- Add useFindParentValues
- Fix form submit await
- Add status adapter and useFetch loading
- Adjust form item render order
- Add pagination expose
- Fix upload file
- Add useFindPropertyRecursive
- upgrade
- Fix beforeSubmit

## 4.2.18

### Patch Changes

- datetime and pagination search
- empty zero
- e
- add directive
- exuploader fixed disabled

## 4.2.17

### Patch Changes

- datetime and pagination search
- empty zero
- e
- exuploader fixed disabled

## 4.2.16

### Patch Changes

- datetime and pagination search
- empty zero
- e

## 4.2.15

### Patch Changes

- empty zero
- e

## 4.2.14

### Patch Changes

- empty zero

## 5.0.0

### Major Changes

- useTimestampFormat return empty when 0

### Patch Changes

- page
- select
- pagination add pullrefresh
- form help
- d
- fixed format date
- page
- useFormFormat
- timestampformat return empty when 0
- rate default null
- use Dayjs
- select
- upload format
- page

## 5.0.0

### Major Changes

- useTimestampFormat return empty when 0

### Patch Changes

- page
- select
- pagination add pullrefresh
- form help
- d
- fixed format date
- page
- useFormFormat
- rate default null
- use Dayjs
- select
- upload format
- page

## 4.2.13

### Patch Changes

- page
- select
- pagination add pullrefresh
- form help
- d
- fixed format date
- page
- useFormFormat
- rate default null
- use Dayjs
- select
- upload format
- page

## 4.2.12

### Patch Changes

- page
- select
- pagination add pullrefresh
- form help
- d
- fixed format date
- page
- useFormFormat
- rate default null
- use Dayjs
- select
- page

## 4.2.11

### Patch Changes

- page
- select
- pagination add pullrefresh
- d
- fixed format date
- page
- useFormFormat
- rate default null
- use Dayjs
- select
- page

## 4.2.10

### Patch Changes

- page
- select
- pagination add pullrefresh
- d
- page
- useFormFormat
- rate default null
- use Dayjs
- select
- page

## 4.2.9

### Patch Changes

- select
- pagination add pullrefresh
- d
- page
- useFormFormat
- rate default null
- use Dayjs
- select
- page

## 4.2.8

### Patch Changes

- select
- pagination add pullrefresh
- d
- page
- useFormFormat
- rate default null
- use Dayjs
- select

## 4.2.7

### Patch Changes

- select
- pagination add pullrefresh
- d
- useFormFormat
- rate default null
- use Dayjs
- select

## 4.2.6

### Patch Changes

- select
- d
- useFormFormat
- rate default null
- use Dayjs
- select

## 4.2.5

### Patch Changes

- select
- d
- useFormFormat
- rate default null
- use Dayjs

## 4.2.4

### Patch Changes

- d
- useFormFormat
- rate default null
- use Dayjs

## 4.2.3

### Patch Changes

- d
- useFormFormat
- use Dayjs

## 4.2.2

### Patch Changes

- d
- use Dayjs

## 4.2.1

### Patch Changes

- d
