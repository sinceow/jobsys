export default {
	components: [
		"./components/button/ExButton.jsx",
		"./components/form/ExForm.jsx",
		"./components/provider/ExProvider.jsx",
		"./components/search/ExSearch.jsx",
		"./components/uploader/ExUploader.jsx",
		"./components/sector/ExSector.jsx",
		"./components/decorator/ExDecorator.jsx",
	],
	outDir: "docs", // folder to save components docs in (relative to the current working directry)
	apiOptions: {
		jsx: true, // tell vue-docgen-api that your components are using JSX to avoid conflicts with TypeScript <type> syntax
	},
}
